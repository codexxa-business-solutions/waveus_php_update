<!doctype html>
<html lang="en" class="no-js">
<head>
	<title>4Ci - Content, Caching, Computing, Communication</title>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<style>
		.page-banner h2 {
    float: none!important;
}
	</style>
	<?php include("header.php");?>

		<!-- content 
			================================================== -->
		<div id="content">

				 <!-- slider 
			================================================== -->
			<div id="slider">
			<div class="flexslider">
				<ul class="slides">
					<li>
						<img alt="" src="upload/4ciBanner.jpg" />
					</li>
				</ul>
		    </div>
		</div>
		<!-- End slider -->

			<div class="fullwidth-box">
				<div class="container">

					<!-- about box-->
					<div class="about-box">
						<div class="row">
							<div class="col-md-12">
								<div class="about-us-text">
									<div class="innner-box">
										<p style="text-align:justify"><span>Computing on the</span>   North bound Interface on Cloud Computing or South bound Interface with Edge Computing is depending on the latency requirement of the Decision Tree's. We offer combined approach which splits the computing into multiple Micro Services to run on both the Interface for Smarter Solution. The Edge and Cloud Computing powered with Network Function Virtualization (NFV), Open Network Orchestration Platform (ONAP) running Micro Services on Docker Images makes the Solution running on Efficient, Smarter and Secured Computing. The smarter application computing runs on Network Slicing and Each Slice Could run multiple Micro Service which scales on Application Computing demands. The Cross-layer optimized System enables efficient utilization of Processing Engines, Connectivity and Reduces the Latencies to enable the faster decisions. The Micro Services within Slices runs combinations of Processing Engines like DSP / FPGA for Signal Processing Applications, General Purpose CPU's for Common Applications, and ASIC running for IoT and Specialized Processing.</p>							
									</div>
								</div>
							</div>
						</div>
					</div>
					<!-- faqs-box -->
				</div>
			</div>
		</div>
		<!-- End content -->

		<?php include("footer.php");?>
	</div>
	<!-- End Container -->
 
</body>

</html>