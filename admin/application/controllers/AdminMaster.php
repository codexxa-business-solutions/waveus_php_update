<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class AdminMaster extends CI_Controller {
		private $_data = array();
	function __construct() {
		parent::__construct();
		$this->load->library('form_validation');
		$this->load->model('Master_model');
		$this->load->helper('cookie');
	}
	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/AdminMaster
	 *	- or -
	 * 		http://example.com/index.php/AdminMaster/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/AdminMaster/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.#b791ff
	 */
	public function index($url = "")
	{
		if(get_cookie('remember_me')=='yes' && get_cookie('userType')=='agent')
		{
			set_cookie("username", $this->input->post("username"), time() + 31536000);
			set_cookie("password", $this->input->post("password"), time() + 31536000);
			set_cookie("userType", $response->type, time() + 31536000);
			set_cookie("userId", $this->input->post("password"), time() + 31536000);
			set_cookie("created_by_userId", $response->id, time() + 31536000);
			set_cookie("remember_me", "yes", time() + 31536000);
			$newdata = array(
									'emailId' => get_cookie('username'),
									'userName' => get_cookie('username'),
									'userType' => get_cookie('userType'),
									'userId' => get_cookie('userId'),
									'created_by_userId' => get_cookie('created_by_userId'),
							);
			$this->session->set_userdata($newdata);
			redirect($this->config->item('base_url').'AdminMaster/dashboard');
		}
		else
		{
			if($this->session->userdata('userId') !='')
			{
				redirect($this->config->item('base_url').'AdminMaster/dashboard');
			}
			else
			{
				$this->load->view('login');	
			}
		}
	}
	
	function login()
	{
		if($this->input->post("action") == "login") 
		{
			$form_field	=	array(
									'username'=> '',
									'password'=>''
								);

            $this->form_validation->set_rules('username', 'Username', 'required');
            $this->form_validation->set_rules('password', 'Password', 'required');
            if ($this->form_validation->run() == FALSE)
			{
				$data['L_strErrorMessage'] = $this->validation->error_string;
				redirect($this->config->item('base_url').'AdminMaster/login');
			}
			else 
			{
				if($this->input->post("username"))                             
				{
					$newdata = array(
								   'username'  => $this->input->post("username"),
								   'password'  => ($this->input->post("password"))
							   );
						if($response = $this->Master_model->checkLogin($newdata)) 
						{
							// for super admin
							$newdata = array(
											   'emailId' => $response->email,
											   'userName' => $response->username,
											   'userType' => $response->type,
											   'userId' => $response->id,
											   'created_by_userId' => $response->created_by_user_id,
											 );
							$this->session->set_userdata($newdata);
							
							//if login successfull and remember me checkbox is checked then set cookie for remember me
							if($this->input->post('remember_me')=='yes')
							{
								set_cookie("username", $this->input->post("username"), time() + 31536000);
								set_cookie("password", $this->input->post("password"), time() + 31536000);
								set_cookie("userType", $response->type, time() + 31536000);
								set_cookie("userId", $this->input->post("password"), time() + 31536000);
								set_cookie("created_by_userId", $response->id, time() + 31536000);
								set_cookie("remember_me", "yes", time() + 31536000);
							}
							redirect($this->config->item('base_url').'AdminMaster/dashboard');
						}
						else
						{
							foreach($form_field as $key => $value)	{	$data[$key]=$this->input->post($key);	}
							$data['L_strErrorMessage'] = "Invalid Username or Password.";
							$this->load->view('login',$data);
						}
				}
				else 
				{
					$this->load->view('login',$data);
				}
			}
		}
		else 
		{
			redirect($this->config->item('base_url').'AdminMaster/login');
		}
	}
	
	public function dashboard()
	{
		if($this->session->userdata('userId') == '')
		{
			redirect($this->config->item('base_url'));
		}
		else
		{
			$this->load->view('dashboard');
		}
	}
	
	function logout()
	{
		if(get_cookie('remember_me')=='yes' && get_cookie('userType')=='agent')
		{
			set_cookie("username", $this->input->post("username"), time() + 31536000);
			set_cookie("password", $this->input->post("password"), time() + 31536000);
			set_cookie("userType", $response->type, time() + 31536000);
			set_cookie("userId", $this->input->post("password"), time() + 31536000);
			set_cookie("created_by_userId", $response->id, time() + 31536000);
			set_cookie("remember_me", "yes", time() + 31536000);
			$newdata = array(
									'emailId' => get_cookie('username'),
									'userName' => get_cookie('username'),
									'userType' => get_cookie('userType'),
									'userId' => get_cookie('userId'),
									'created_by_userId' => get_cookie('created_by_userId'),
							);
			$this->session->set_userdata($newdata);
			redirect($this->config->item('base_url').'AdminMaster/dashboard');
		}
		else
		{
			$form_field['e_datetime'] = date("Y-m-d H:i:s");
			if($response = $this->Master_model->updateData('user_log','id = '.$this->session->userdata('log_id'), $form_field)) 
			{
				$this->session->sess_destroy();
				$this->load->view('login');
			}
			else 
			{
				redirect($this->config->item('base_url'));
			}
		}
		
	}
	
	function changePassword()
	{
		if($this->input->post("action")=="changePassword")
		{  

					if($old_data = $this->Master_model->getData('user_master',array('password' => $this->input->post('old_password'))))
					{
				
								$data['password'] = $this->input->post('new_password');
								$data['updated_date'] = date("Y-m-d H:i:s");
																
								if($response = $this->Master_model->updateData('user_master','id = '.$old_data[0]->id, $data))
								{
									$this->session->set_flashdata('MSG','Password Updated Successfully!!!');
									$res_arrr['status'] = 'success';
									$res_arrr['msg'] = "Video Updated Successfully!!!";
									echo json_encode($res_arrr);exit;
								}
								else 
								{
									$this->session->set_flashdata('ERROR','Some Errors prevented from adding data,please try later.');
									$res_arrr['status'] = 'fail';
									$res_arrr['msg'] = "Some Errors prevented from adding data,please try later.";
									echo json_encode($res_arrr);exit;
								}
					}
					else
					{
						$this->session->set_flashdata('ERROR','Old password cannot be matched, please try again.');
						$res_arrr['status'] = 'wrong';
						$res_arrr['msg'] = "Old password cannot be matched, please try again.";
						echo json_encode($res_arrr);exit;
					}
						
		}else{
			$this->load->view('change_password');	
		}
	}
	
}
?>