<?php 
defined('BASEPATH') or exit('NO DIRECT SCRIPT ACCESS');

class CandidateMaster extends CI_Controller{
	  function __construct(){
		parent:: __construct();
		 
		  $this->load->model('Master_model');
		  $this->load->helper('cookie');
	  }
 
	public function listCandidate()
	{
		$data['list_candidate'] = $this->Master_model->getData('candidate_master');
		$this->load->view('list_candidate',$data);
	}
		
}
?>