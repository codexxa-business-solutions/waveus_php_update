<?php 
defined('BASEPATH') or exit('NO DIRECT SCRIPT ACCESS');

class CareerMaster extends CI_Controller{
	  function __construct(){
		parent:: __construct();
		 
		  $this->load->model('Master_model');
		  $this->load->helper('cookie');
	  }
 
	public function listCareer()
	{
		$data['list_career'] = $this->Master_model->getData('career_master');
		$this->load->view('list_career',$data);
	}
	
	public function addCareer()
	{
		$this->load->view('add_career');		
	}
	
	public function addCareerFunction()
	{
		if($this->input->post("action")=="addCareer")
		{
			$this->form_validation->set_rules('testi_name','Name','trim|required');
			
			if($this->form_validation->run() == FALSE)
			{
				echo json_encode(['status'=>'fail','msg'=>$this->form_validation->error_array()]);exit;
			}
		 	else
		 	{
				$form_field = $data = array(
								'title' => '',
								'testi_name' => '',
								'content' => '',
							);  

						foreach($form_field as $key => $value)	
						{
							$data[$key]=$this->input->post($key);
						}
							
							
							
							$data['added_on'] = date('Y-m-d H:i:s');
							
							if($response = $this->Master_model->addData('career_master',$data))
							{
								$id = $response;
								$this->session->set_flashdata('MSG','Career Added Successfully');
								echo json_encode(['status'=>'success','id'=>$id]);exit;
							}
							else 
							{
								$this->session->set_flashdata('ERROR','Some error occured.');
								echo json_encode(['status'=>'fail']);exit;
							}
			}
			
		}
	}
	
	public function editCareer($id="")
	{
		if(is_numeric($id))
		{
			if($this->input->post("action")=="editCareer")
			{
				$this->form_validation->set_rules('testi_name','Name','trim|required');
				
				if($this->form_validation->run() == FALSE)
				{
					echo json_encode(['status'=>'fail','msg'=>$this->form_validation->error_array()]);exit;
				}
				else
				{
					$form_field = $data = array(
									'title' => '',
									'testi_name' => '',
									'content' => '',
								);  

							foreach($form_field as $key => $value)	
							{
								$data[$key]=$this->input->post($key);
							}
							
						
							
						if($response = $this->Master_model->updateData('career_master','t_id = '.$id, $data))
						{
							$this->session->set_flashdata('MSG','Career Updated Successfully!!!');
							$res_arrr['status'] = 'success';
							$res_arrr['msg'] = "Career Updated Successfully!!!";
							echo json_encode($res_arrr);exit;
						}
						else 
						{
							$this->session->set_flashdata('ERROR','Some Errors prevented from adding data,please try later.');
							$res_arrr['status'] = 'fail';
							$res_arrr['msg'] = "Some Errors prevented from adding data,please try later.";
							echo json_encode($res_arrr);exit;
						}	
							
					}
			}
			else
			{
				$data['career_data'] = $this->Master_model->getData('career_master',array('t_id'=>$id));
				$this->load->view('edit_career',$data);
			}
		}
		else
		{
			redirect($this->config->item('base_url').'CareerMaster/listCareer');			
		}
	}
	
	
	function deleteCareer($id)
	{
		//check if id exist
		 $result = $this->Master_model->getData('career_master',array('t_id'=>@$id));
		 if(@$result[0]->t_id!='')
		 {
			 $condition = "t_id = ".@$id." ";
			 $response = $this->Master_model->deleteData('career_master',$condition);
			 if(@$response)
			 {
				 $this->session->set_flashdata('MSG','Career Deleted successfully.');
				 redirect($this->config->item('base_url').'CareerMaster/listCareer');
			 }
			 else
			 {
				 $this->session->set_flashdata('ERROR','Invalid Career id.');
				 redirect($this->config->item('base_url').'CareerMaster/listCareer');
			 }
		 }
		 else
		 {
			 $this->session->set_flashdata('ERROR','Invalid Career id.');
			 redirect($this->config->item('base_url').'CareerMaster/listCareer');
		 }
	}
	
	function multiDelete(){
		if($this->input->post("checked_id"))
		{
			foreach($this->input->post("checked_id") as $val)
			{
				$condition = "t_id = ".@$val." ";
				$response = $this->Master_model->deleteData('career_master',$condition);
			}
			
			if(@$response)
			 {
				 $this->session->set_flashdata('MSG','Career(s) Deleted successfully.');
				 redirect($this->config->item('base_url').'CareerMaster/listCareer');
			 }
		}
	}
	

	
}
?>