<?php
class Master_model extends CI_Model {
	private $_data = array();
	function __construct() {
		parent::__construct();
	}
	

	/**
	* This function is used for inser data.
	* Parameter : $tableName [table name], $data [column name and values]
	* check tablename and data is not NULL then insert in table.
	*/	
	function addData($tableName,$data) 
	{
		
		if($tableName != "" and $data != "")
		{
		  $insertData = $this->db->insert($tableName,$data);
		  if($insertData)
		  {
			return $this->db->insert_id();
		  }
		  else
		  {
			return false;
		  }
		}      
	}
	
	
	/**
	* This function is used for update data.
	* Parameter : $tableName [table name], $condition [conditions as array], $columns [column name and values]
	* check tablename and conditions is not NULL then update in table.
	*/
  function updateData($tableName,$condition="",$columns)
  {
		if($tableName != "")
		{
			  if($condition != "")
			  {
				$this->db->where($condition);
			  }
			  if($this->db->update($tableName,$columns))
			  {
			   return true;
			  }
			  else{
				return false;
			  }
		}
		else
		{
		  return false;
		}
  }
	
  /**
	* This function is used for delete data.
	* Parameter : $tableName [table name], $condition [conditions as array]
	* check tablename and conditions is not NULL then delete row in table.
	*/
  function deleteData($tableName,$condition="")
  {
		if($tableName != "")
		{
			  if($condition != '')
			  {
				$this->db->where($condition);
			  }
			  if($this->db->delete($tableName))
			  {
				return true;
			  }
			  else
			  {
				return false;
			  }
		}
  }
    
  /**
	* This function is used for deactive data.
	* Parameter : $tableName [table name], $condition [conditions as array]
	* check tablename and conditions is not NULL then deactive row in table.
	*/
  function deactiveData($tableName,$condition="",$columns)
  {
		if($tableName != "")
		{
			  if($condition != '')
			  {
				$this->db->where($condition);
			  }
			  if($this->db->update($tableName,$columns))
			  {
				return true;
			  }
			  else
			  {
				return false;
			  }
		}
  }
	

  /**
	* This function is used for get data.
	* Parameter : $tableName [table name], $condition [conditions as array], $columns [column name and values], $order_by_col [order by wich column]
	* check tablename and conditions is not NULL then select rows table.
	*/
  function getData($tableName,$condition='',$columns='',$order_by_col='',$order_status='',$offset='',$limit='')
  {
    if($tableName != "" or $tableName != null)
    {
	  if ($order_by_col != "") 
	  {
			$this -> db -> order_by($order_by_col, $order_status);
	  }
      if($columns != '')
      {
        $data = $this->db->select($columns);
      }
      if($limit != '')
      {
          $this->db->limit($offset,$limit);
      }
      if($condition != '')
      {
        $data = $this->db->get_where($tableName,$condition);
      }
      else
      {
        $data = $this->db->get($tableName);
      }
      if($data->num_rows() > 0)
      {
        $data = $data->result();
        return $data;
      }
      else
      {
        return false;
      }
    }
    else
    {
      return;
    }
  }
  
  
  /**
	* This function is used for get data count.
	* Parameter : $tableName [table name], $condition [conditions as array], $columns [column name and values], $order_by_col [order by wich column]
	* check tablename and conditions is not NULL then select rows table.
	*/
  function getDataCount($tableName,$condition='',$columns='',$order_by_col='',$offset='',$limit='')
  {
    if($tableName != "" or $tableName != null)
    {
	  if ($order_by_col != "") 
	  {
			$this -> db -> order_by($order_by_col, "asc");
	  }
      if($columns != '')
      {
        $data = $this->db->select($columns);
      }
      if($limit != '')
      {
          $this->db->limit($offset,$limit);
      }
      if($condition != '')
      {
        $data = $this->db->get_where($tableName,$condition);
      }
      else
      {
        $data = $this->db->get($tableName);
      }
      if($data->num_rows() > 0)
      {
        $data = $data->num_rows();
        return $data;
      }
      else
      {
        return false;
      }
    }
    else
    {
      return;
    }
  }

  function getCountData($tableName,$condition='',$columns='',$order_by_col='')
  {
    if($tableName != "" or $tableName != null)
    {
	  if ($order_by_col != "") 
	  {
			$this -> db -> order_by($order_by_col, "asc");
	  }
      if($columns != '')
      {
        $data = $this->db->select($columns);
      }
      if($condition != '')
      {
        $data = $this->db->get_where($tableName,$condition);
      }
      else
      {
        $data = $this->db->get($tableName);
      }
      if($data->num_rows() > 0)
      {
        $data = $data->num_rows();
        return $data;
      }
      else
      {
        return false;
      }
    }
    else
    {
      return;
    }
  }
  
/**
	* This function is used for checking logins.
	* Parameter : $data
	* check tablename and conditions is not NULL then select rows table.
*/  
 function checkLogin($data)
 {
		//http://formvalidation.io/examples/jquery-steps/
		$query = $this->db->get_where('user_master', array('email' => $_POST['username']));
		$data = $query->row_array();
		// $s_password = $this->encrypt->decode($data['password']);
		$s_password = $data['password'];
		$user_pass = $this->input->post('password');
		if ($query->num_rows() > 0 && $data['is_active']=='TRUE')	
		{
			if($s_password==$user_pass)
			{
				$row = $query->row();
				$row1 = $query->row_array();
				
				//if success then store log
				$data1['user_id'] = $row1['id'];
				$data1['email'] = $row1['email'];
				$data1['s_datetime'] = date("Y-m-d H:i:s");
				$data1['login_ip'] = $_SERVER['REMOTE_ADDR'];
				$addr_details = unserialize(file_get_contents('http://www.geoplugin.net/php.gp?ip='.$_SERVER['REMOTE_ADDR']));
				$country = stripslashes(ucfirst($addr_details['geoplugin_countryName']));
				$city = stripslashes(ucfirst($addr_details['geoplugin_city']));
				$data1['login_location'] = $country.", ".$city;
				$data1['logtype'] = $row1['type'];
				$data1['browser'] = $_SERVER['HTTP_USER_AGENT'];
				$insertData = $this->db->insert('user_log',$data1);
				  if($insertData)
				  {
					  $newdata = array(
											   'log_id' =>$this->db->insert_id(),
											 );
					$this->session->set_userdata($newdata);
					return $row;
				  }
				  else
				  {
					return false;
				  }
			}
			else
			{
				return false;
			}
		} 
		else 
		{
			return false;
		}
}

/**
	* This function is used for checking Front logins.
	* Parameter : $data
	* check tablename and conditions is not NULL then select rows table.
*/  
 function checkFrontLogin($data)
 {
		//http://formvalidation.io/examples/jquery-steps/
		$query = $this->db->get_where('doctor_master', array('email' => $_POST['email']));
		$data = $query->row_array();
		// $s_password = $this->encrypt->decode($data['password']);
		$s_password = $data['password'];
		$user_pass = $this->input->post('password');
		if ($query->num_rows() > 0)	
		{
			if($s_password==$user_pass)
			{
				$row = $query->row();
				$row1 = $query->row_array();
				
				//if success then store log
				$data1['user_id'] = $row1['doc_id'];
				$data1['email'] = $row1['email'];
				
				$data1['s_datetime'] = date("Y-m-d H:i:s");
				$data1['login_ip'] = $_SERVER['REMOTE_ADDR'];
				$addr_details = unserialize(file_get_contents('http://www.geoplugin.net/php.gp?ip='.$_SERVER['REMOTE_ADDR']));
				$country = stripslashes(ucfirst($addr_details['geoplugin_countryName']));
				$city = stripslashes(ucfirst($addr_details['geoplugin_city']));
				$data1['login_location'] = $country.", ".$city;
				//$data1['logtype'] = $row1['type'];
				$data1['browser'] = $_SERVER['HTTP_USER_AGENT'];
				$insertData = $this->db->insert('front_user_log',$data1);
				  if($insertData)
				  {
					  $newdata = array(
											   'log_id' =>$this->db->insert_id(),
											 );
					$this->session->set_userdata($newdata);
					return $row;
				  }
				  else
				  {
					return false;
				  }
			}
			else
			{
				return false;
			}
		} 
		else 
		{
			return false;
		}
}

//function to get 
function checkData($table,$data){
    if($table == ''){
      return ['status'=>'FAIL','msg'=>'Could Not Process'];
    }
    else{
      $query = $this->db->get_where($table,$data);
      $count = $query->num_rows();
      if($count ==1){
        return ['status'=>'PASS','msg'=>'Data Exists','count'=>$count];
      }
      elseif($count > 1){
        return ['status'=>'PASSBUT','msg'=> $count.' Data Found For Provided Details','count'=>$count];
      }
      else{
        return ['status'=>'PASSEDBUT','msg'=>'No Data Found'];
      }
    }
}

//function to get last inserted id from table
function getLastInsertedId($table,$column)
{
		$query ="select ".$column." from ".$table." order by ".$column." DESC limit 1";
		$res = $this->db->query($query);
		if($res->num_rows() > 0) 
		{
            return $res->result("array");
		}
		return array();
}

function getSearchData($params = array())
  {
		  $this->db->select('dm.*,dim.*');
		  $this->db->from('doctor_master as dm , doctor_insurance_master as dim');
		  $this->db->where('dm.doc_id=dim.doctor_id');
		  
		  if(array_key_exists("specialty",$params)){
				$this->db->where("dm.specialty = '".$params['specialty']."'");
           }
		   if(array_key_exists("insurance",$params)){
			$this->db->where("dim.insurance_name = '".$params['insurance']."'");
           }

			if(array_key_exists("city_zip",$params)){
				$where = "(dm.city = '".$params['city_zip']."' OR dm.zip_code = '".$params['city_zip']."')";
				$this->db->where($where);
			//$this->db->where("dm.city = '".$params['city_zip']."'");
           // }
		   
		   // if(array_key_exists("city_zip",$params)){
			// $this->db->where("dm.zip_code = '".$params['city_zip']."'");
           }
		   
		  $this->db->group_by('dm.doc_id'); 
      
         
      
		  if(array_key_exists("name",$params)){
				$this->db->like(array('dm.first_name' => $params['name']));
			   }
		   
		   if(array_key_exists("plan",$params)){
            $this->db->like(array('dim.plan_name' => $params['plan']));
           } 
            
         
		  $data = $this->db->get();
		    if($data->num_rows() > 0)
			{
				$data = $data->result();
				return $data;
			}
			else
			{
				return false;
			}
  }
  
  public function dataSearch($params = array())
  {
					
			$this->db->select('*');
            $this->db->from('doctor_master');
           // $this->db->join('doctor_insurance_master as dim', 'dim.doctor_id = dm.doc_id', 'left');
           // $this->db->where('dm.doc_id=dim.doctor_id');
					   
		    if(array_key_exists("specialty",$params) && $params['specialty'] != ''){
				$this->db->where("specialty = '".$params['specialty']."'");
           }
		   if(array_key_exists("state",$params) && $params['state'] != ''){
				$this->db->where("state = '".$params['state']."'");
           }
		   if(array_key_exists("bio",$params)  && $params['bio'] != ''){
			$this->db->where("bio LIKE '%".$params['bio']."%' ");
           }

			// if(array_key_exists("city_zip",$params) && $params['city_zip'] != ''){
				// $where = "(city = '".$params['city_zip']."' OR zip_code = '".$params['city_zip']."')";
				// $this->db->where($where);						
           // }
		   
		   if(array_key_exists("email",$params) && $params['email'] != ''){
				$this->db->where("email = '".$params['email']."'");						
           }
		   
		   
            //$this->db->group_by('dm.doc_id');
			
	  
	  if(array_key_exists("name",$params) && $params['name'] != ''){
            $this->db->like(array('first_name' => $params['name']));
            //$this->db->or_like(array('dm.last_name' => $params['name']));
           }
		   
			
			$data = $this->db->get();
		    if($data->num_rows() > 0)
			{
				$data = $data->result();
				return $data;
			}
			else
			{
				return false;
			}
  }
  
}
?>