<?php 
include('includes/header.php');
include('includes/sidebar.php');
?>
<section id="main" role="main">
      <!-- START Template Container -->
    <div class="container-fluid">
    <!-- START row -->
    <div class="">
          <div class="col-md-12">
            <!-- START panel -->
			      <ol class="breadcrumb">
					<li><a href="<?php echo $base_url; ?>AdminMaster/dashboard">Dashboard</a></li>
					<li><a href="<?php echo $base_url; ?>CareerMaster/listCareer">List Career(s)</a></li>
					<li class="active">Add Career</li>
                  </ol>
				  
			<div class="panel panel-default">
              <!-- panel heading/header -->
              <div class="panel-heading genrl">
                <h3 class="panel-title">Add Career</h3>
              </div>
			   <div class="alert" role="alert"></div>
              <!--/ panel heading/header -->
              <!-- panel body -->
			<div class="panel-body">
				<form action="javascript:void(0)" method="post" class="form-horizontal form-bordered" enctype="multipart/form-data" id="TestimonialForm">
					<input type="hidden" name="action" id="detail_action" value="addCareer">       
					<input type="hidden" id="url" value="<?php echo $base_url; ?>CareerMaster/addCareerFunction"> 
					<input type="hidden" id="redirecturl" name="redirecturl" value="<?php echo $base_url; ?>CareerMaster/listCareer"> 
									
				 <div class="col-12 col-sm-12 col-lg-12">
					<div class="row setup-content" id="step-1">
						<div class="col-md-12">
								<div class="row">
											
									<div class="form-group">
									<label class="col-sm-2 control-label">Job Title<span class="inner-star">*</span></label>
										<div class="col-sm-4 col-lg-4">
										  <input type="text" name="title" id="title" class="form-control">
										</div>
									</div>
											
									<div class="form-group">
									<label class="col-sm-2 control-label">Job Descriptions<span class="inner-star">*</span></label>
										<div class="col-sm-6 col-lg-6">
										  <textarea name="testi_name" id="testi_name" class="form-control"></textarea>
										</div>
									</div>
											
									<div class="form-group">
									<label class="col-sm-2 control-label">Desired Skills<span class="inner-star">*</span></label>
										<div class="col-sm-6 col-lg-6">
										  <textarea name="content" id="content" class="form-control"></textarea>
										</div>

									</div>
											
								</div>
							<button class="btn btn-primary submit pull-right detail_next" id="ImageFormBtn" type="submit" >Save</button>
						</div>
					</div>
					</div>
				</form>
			</div>
			</div>
  </div>
      <!-- START To Top Scroller -->
      <a data-offset="50%" data-hideanim="bounceOut" data-showanim="bounceIn" data-toggle="waypoints totop" class="totop animation" href="#">
        <i class="fa fa-chevron-up"></i>
      </a>
      <!--/ END To Top Scroller -->
	</div>
</div>
</div></section>
<?php 
include('includes/footer.php');
?>
<!-- ckeditor -->
<script type="text/javascript" src="<?php echo $base_adminurl_views; ?>js/TestimonialMaster.js"></script>
<script type="text/javascript">
$(document).ready(function () {
	initTestimonial('<?php echo $base_url; ?>','<?php echo @$base_adminurl_views; ?>');
});
</script>
