<?php 
include('includes/header.php');
include('includes/sidebar.php');
?>
<section id="main" role="main">
      <!-- START Template Container -->
    <div class="container-fluid">
    <!-- START row -->
    <div class="">
          <div class="col-md-12">
            <!-- START panel -->
			      <ol class="breadcrumb">
					<li><a href="<?php echo $base_url; ?>AdminMaster/dashboard">Dashboard</a></li>
					
					<li class="active">Change Password</li>
                  </ol>
				  
			<div class="panel panel-default">
              <!-- panel heading/header -->
              <div class="panel-heading genrl">
                <h3 class="panel-title">Change Password</h3>
              </div>
			   <div class="alert" role="alert"></div>
              <!--/ panel heading/header -->
			  <?php 
					if($this->session->flashdata('MSG')){
                ?>
               <div class="alert alert-success fade in">
				<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                 <?php
					echo $this->session->flashdata('MSG');
                ?>
              </div>
              <?php
                }
                 ?>
              
                 <?php 
              if($this->session->flashdata('ERROR')){
                ?>
              <div class="alert alert-danger fade in">
				<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                <?php
					echo $this->session->flashdata('ERROR');
                ?>
              </div>
              <?php 
              }
              ?>
              <!-- panel body -->
			<div class="panel-body">
				<form action="javascript:void(0)" method="post" class="form-horizontal form-bordered" enctype="multipart/form-data" id="ChangePasswordForm">
					<input type="hidden" name="action" id="detail_action" value="changePassword">       
					<input type="hidden" id="url" value="<?php echo $base_url; ?>AdminMaster/changePassword"> 
					<input type="hidden" id="redirecturl" name="redirecturl" value="<?php echo $base_url; ?>AdminMaster/changePassword"> 
									
				 <div class="col-12 col-sm-12 col-lg-12">
					<div class="row setup-content" id="step-1">
						<div class="col-md-12">
								<div class="row">
																			
									<div class="form-group">
									<label class="col-sm-2 control-label">Old Password<span class="inner-star">*</span></label>
										<div class="col-sm-4 col-lg-4">
										  <input type="password" name="old_password" id="old_password" class="form-control">
										</div>

									</div>
											
									<div class="form-group">									
											<label class="col-sm-2 control-label">New Password<span class="inner-star">*</span></label>
										<div class="col-sm-4 col-lg-4">
										  <input type="password" name="new_password" id="new_password" class="form-control">
										</div>
									</div>
									
									<div class="form-group">									
											<label class="col-sm-2 control-label">Retype New Password<span class="inner-star">*</span></label>
										<div class="col-sm-4 col-lg-4">
										  <input type="password" name="re_new_password" id="re_new_password" class="form-control">
										</div>
									</div>
									
									
								</div>
							<button class="btn btn-primary submit pull-right detail_next" id="ImageFormBtn" type="submit" >Save</button>
						</div>
					</div>
					</div>
				</form>
			</div>
			</div>
  </div>
      <!-- START To Top Scroller -->
      <a data-offset="50%" data-hideanim="bounceOut" data-showanim="bounceIn" data-toggle="waypoints totop" class="totop animation" href="#">
        <i class="fa fa-chevron-up"></i>
      </a>
      <!--/ END To Top Scroller -->
	</div>
</div>
</div></section>
<?php 
include('includes/footer.php');
?>