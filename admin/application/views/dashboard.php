<?php 
include('includes/header.php'); ?>
<?php include('includes/sidebar.php'); ?>
<section id="main" role="main">
<div class="container-fluid">
        <!-- Page Header -->
		<!-- START Template Container -->
        <!-- START row -->
        <div class="row">
          <div class="col-md-12">
            <!-- START panel -->
            <div class="panel panel-default">
              <!-- panel heading/header -->
              <div class="panel-heading genrl">
                <h3 class="panel-title">Dashboard</h3>
              </div>
              <!--/ panel heading/header -->
              <!-- panel body -->
              <div class="panel-body">
			  <div class="row">
			<div class="col-lg-12" style="padding-top: 10px">
				<div class="widget inner">
					<div class="metrial-min">
						<div class="row">
							<div class="col-md-3">
								<div class="list-group">              
							 <?php
							 $list_images = $this->Master_model->getData('image_master');
							 
							 ?>
									<a href="<?php echo $base_url; ?>ImageMaster/listImage" class="list-group-item twitter">
									
										<h3 class="pull-right">
											<!--<i class="fa fa-twitter-square"></i>-->
										</h3>
										<h4 class="list-group-item-heading count countCricket">
											</h4>
										<p class="list-group-item-text">
											Number Of Image(s)&nbsp;:&nbsp;<?php echo count(@$list_images); ?>
										</p>
									</a>
								</div>
							</div>
							
						</div>
                    </div>
                    </div>
                    </div>
              </div>
              <!-- panel body -->
            </div>
            <!--/ END form panel -->
        <!--/ END row -->
      <!-- START To Top Scroller -->
      <a data-offset="50%" data-hideanim="bounceOut" data-showanim="bounceIn" data-toggle="waypoints totop" class="totop animation" href="#">
        <i class="fa fa-chevron-up"></i>
      </a>
      <!--/ END To Top Scroller -->
    </section>  
        <!-- Page Header -->
</div>
      <!--/ END Template Container -->
      <!-- START To Top Scroller -->
      <a href="#" class="totop animation" data-toggle="waypoints totop" data-showanim="bounceIn" data-hideanim="bounceOut" data-offset="50%">
        <i class="ico-angle-up"></i>
			</a></div></div>
</section>
<?php include('includes/footer.php'); ?>