<footer id="footer">
      <!-- START container-fluid -->
      <div class="container-fluid">
        <div class="row">
          <div class="col-sm-8 ">
            <!-- copyright -->
            <p class="nm text-muted">© Waveus Network Pvt Ltd. All Rights Reserved.</p>
            <!--/ copyright -->
          </div>
          
        </div>
      </div>
      <!--/ END container-fluid -->
    </footer>
	
	<script type="text/javascript" src="<?php echo $base_adminurl_views; ?>js/vendor.js"></script>
    <script type="text/javascript" src="<?php echo $base_adminurl_views; ?>js/core.js"></script>		
	<script type="text/javascript" src="<?php echo $base_adminurl_views; ?>js/datatables.min.js"></script>
    <script type="text/javascript" src="<?php echo $base_adminurl_views; ?>js/dataTables.responsive.min.js"></script>
	<script type="text/javascript" src="<?php echo $base_adminurl_views; ?>js/jquery.validate.js"></script>
	<script type="text/javascript" src="<?php echo $base_adminurl_views; ?>js/additional-methods.min.js"></script>
	<script type="text/javascript" src="<?php echo $base_adminurl_views; ?>js/jquery-ui.min.js"></script>
	<script type="text/javascript" src="<?php echo $base_adminurl_views; ?>js/bootstrap-timepicker.js"></script>
	<script type="text/javascript" src="<?php echo $base_adminurl_views; ?>js/jquery.classyscroll.js"></script>
	<script type="text/javascript" src="<?php echo $base_adminurl_views; ?>js/jquery.mousewheel.js"></script>
	<style>.scrollbarsin{width:100%;height:130px}</style>
	<script type="text/javascript" src="<?php echo $base_adminurl_views; ?>js/commonJs.js"></script>
    <div style="display: none; position: absolute; background: rgb(255, 255, 255) none repeat scroll 0% 0%; z-index: 1040; padding: 0.4em 0.6em; border-radius: 0.5em; font-size: 0.8em; border: 1px solid rgb(17, 17, 17); white-space: nowrap;" class="flotTip">
    </div>