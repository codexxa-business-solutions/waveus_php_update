<?php
$front_base_url = $this->config->item('front_base_url');
$base_url 		= $this->config->item('base_url');
$http_host 		= $this->config->item('http_host');
$base_url_views = $this->config->item('base_url_views');
$base_adminurl_views = $this->config->item('base_adminurl_views');
$base_upload = $this->config->item('upload');
$view_url = $this->config->item('view_url');
$this->load->helper('cookie');
?>
<!DOCTYPE html>
<html style="" class="backend js flexbox flexboxlegacy canvas canvastext webgl no-touch geolocation postmessage no-websqldatabase indexeddb hashchange history draganddrop websockets rgba hsla multiplebgs backgroundsize borderimage borderradius boxshadow textshadow opacity cssanimations csscolumns cssgradients no-cssreflections csstransforms csstransforms3d csstransitions fontface generatedcontent video audio localstorage sessionstorage webworkers applicationcache svg inlinesvg smil svgclippaths responsejs  screen-lg"><!-- START Head --><head>
<meta http-equiv="content-type" content="text/#b791ff; charset=UTF-8">
    <!-- START META SECTION -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Waveus Network Pvt Ltd</title>
	<link rel="stylesheet" type="text/css" href="<?php echo $base_adminurl_views; ?>css/datatables.min.css"/>
    <link rel="stylesheet" href="<?php echo $base_adminurl_views; ?>css/bootstrap.css">
    <link rel="stylesheet" href="<?php echo $base_adminurl_views; ?>css/jquery.validation.css">
    <link rel="stylesheet" href="<?php echo $base_adminurl_views; ?>css/jquery.classyscroll.css">
    <link rel="stylesheet" href="<?php echo $base_adminurl_views; ?>css/documentation.css">
	<link rel="stylesheet" href="<?php echo $base_adminurl_views; ?>css/bootstrap-timepicker.css">
    <link rel="stylesheet" href="<?php echo $base_adminurl_views; ?>css/font-awesome.css">
    <link rel="stylesheet" href="<?php echo $base_adminurl_views; ?>css/animate.css">
    <link rel="stylesheet" href="<?php echo $base_adminurl_views; ?>css/layoutuse.css">
    <link rel="stylesheet" href="<?php echo $base_adminurl_views; ?>css/uielementuse.css">
    <link rel="stylesheet" href="<?php echo $base_adminurl_views; ?>css/jquery-ui.min.css">
	<link rel="stylesheet" href="<?php echo $base_adminurl_views; ?>css/jquery.fileupload.css">
	<script type="text/javascript" src="<?php echo $base_adminurl_views; ?>js/jquery-2.1.4.min.js"></script>
  <style type="text/css">					
  .jqstooltip 
  { 
  	position: absolute;left: 0px;
  	top: 0px;visibility: hidden;
  	background: rgb(0, 0, 0) transparent;
  	background-color: rgba(0,0,0,0.6);
  	filter:progid:DXImageTransform.Microsoft.gradient(startColorstr=#99000000, endColorstr=#99000000);
  	-ms-filter: "progid:DXImageTransform.Microsoft.gradient(startColorstr=#99000000, endColorstr=#99000000)";
  	color: white;
  	font: 10px arial, san serif;
  	text-align: left;white-space: nowrap;
  	padding: 5px;
  	border: 1px solid white;
  	box-sizing: content-box;
  	z-index: 10000;
  	}
  	.jqsfield 
  	{ 
	color: white;
  	font: 10px arial, san serif;
  	text-align: left;
  	}
  	</style>	
  	</head>
  <!--/ END Head -->
  <!-- START Body -->
  <body data-baseurl="../../">
    <!-- START Template Header -->
  <header id="header" class="navbar">
      <!-- START container fluid -->
      <div class="container-fluid">
        <!-- START navbar header -->
        <div class="navbar-header">
          <!-- Brand -->
          <a class="navbar-brand" href="" style="background-color:#ddd;">
            <span class="logo-figure"></span>
            <span class="logo-text"></span>
          </a>
          <!--/ Brand -->
        </div>
        <!--/ END navbar header -->
        <!-- START Toolbar -->
        <div class="navbar-toolbar clearfix">
          <!-- START Left nav -->
          <ul class="nav navbar-nav navbar-left">
            <!-- Sidebar shrink -->
            <li class="hidden-xs hidden-sm">
              <a href="" class="sidebar-minimize" data-toggle="minimize" title="Minimize sidebar">
                <span class="meta">
                  <span class="icon"></span>
                </span>
              </a>
            </li>
            <!--/ Sidebar shrink -->
            <!-- Offcanvas left: This menu will take position at the top of template header (mobile only). Make sure that only #header have the `position: relative`, or it may cause unwanted behavior -->
            <li class="navbar-main hidden-lg hidden-md hidden-sm">
              <a href="" data-toggle="sidebar" data-direction="ltr" rel="tooltip" title="Menu sidebar">
                <span class="meta">
                  <span class="icon">
                    <i class="fa fa-outdent"></i>
                  </span>
                </span>
              </a>
            </li>
            <li class="dropdown custom click-span_" id="header-dd-message">
              <input type="hidden" value="<?php echo $base_url; ?>" id="urls">
              <a href="" class="dropdown-toggle" data-toggle="dropdown">
                <span class="meta">
                  
                </span>
              </a>
              <div style="" class="dropdown-menu" role="menu">
                <div class="dropdown-header">
                  <span class="title">Messages
                    <span class="count"></span>
                  </span>
<!--                  <span class="option text-right"><a href="">New message</a></span>-->
                </div>
                <div style="position: relative; overflow: hidden; width: auto;" class="viewport">
                
                <div style="background: rgb(0, 0, 0) none repeat scroll 0% 0%; width: 8px; position: absolute; top: 0px; opacity: 0.4; display: block; border-radius: 7px; z-index: 99; right: 0px;" class="scrollbar"></div><div style="width: 8px; height: 100%; position: absolute; top: 0px; display: none; border-radius: 7px; background: rgb(51, 51, 51) none repeat scroll 0% 0%; opacity: 0.2; z-index: 90; right: 0px;" class="scrollrail"></div></div>
              </div>
              <!--/ Dropdown menu -->
            </li>
            <!--/ Offcanvas left -->   
          </ul>
          <!--/ END Left nav -->
          <!-- START navbar form -->
          <div class="navbar-form navbar-left dropdown" id="dropdown-form">
            <form action="" role="search">
              <div class="has-icon">
                <input class="form-control" placeholder="Search application..." type="text">
                 <i class="fa fa-search form-control-icon"></i>
              </div>
            </form>
          </div>
          <!-- START navbar form -->
          <!-- START Right nav -->
          <ul class="nav navbar-nav navbar-right">
            <!-- Profile dropdown -->
            <li class="dropdown profile">
             <a class="welcome" href="#">
                <span class="meta">
                  <span class="avatar">
                    <img src="<?php echo $base_adminurl_views; ?>img/avatar7.jpg" class="img-circle" alt="">
                  </span> Welcome <?php echo $this->session->userdata('userName'); ?>
                </span>
              </a>
            </li>
			
				<li class="dropdown profile">
				 <a  href="<?php echo $base_url;?>AdminMaster/logout">
					<span class="meta">
					  <span class="text pl5"> <span class="icon">
					   <i class="fa fa-sign-out"></i>
						</span> Sign Out</span>
					</span>
				  </a>
				</li>
			
            <!--/ Offcanvas right -->
          </ul>
          <!--/ END Right nav -->
        </div>
        <!--/ END Toolbar -->
      </div>
      <!--/ END container fluid -->
    </header>
    <!--/ END Template Header -->