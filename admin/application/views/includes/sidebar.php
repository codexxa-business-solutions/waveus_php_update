<?php
$urlSegment = $this->uri->segment(2);
$url = $this->uri->segment(1);
?>
<!-- START Template Sidebar (Left) -->
   <aside class="sidebar sidebar-left sidebar-menu">
      <!-- START Sidebar Content -->  
      <div class="content slimscroll">
        <!--<h5 class="heading">Main Menu</h5>-->
        <!-- START Template Navigation/Menu -->
        <ul class="topmenu topmenu-responsive" data-toggle="menu">
							
					
					
					<li <?php if($url == 'CareerMaster') { ?>class="open active"<?php } ?>>
						<a href="<?php echo $base_url ?>CareerMaster/listCareer">
						<span class="figure">
							<i class="fa fa-tasks"></i>
					   </span>
						  <span class="text">List Career(s)</span>
						</a>
					</li>
					
					<li <?php if($url == 'CandidateMaster') { ?>class="open active"<?php } ?>>
						<a href="<?php echo $base_url ?>CandidateMaster/listCandidate">
						<span class="figure">
							<i class="fa fa-tasks"></i>
					   </span>
						  <span class="text">List Candidate(s)</span>
						</a>
					</li>
					
					<li <?php if($url == 'AdminMaster') { ?>class="open active"<?php } ?>>
						<a href="<?php echo $base_url ?>AdminMaster/changePassword">
						<span class="figure">
							<i class="fa fa-tasks"></i>
					   </span>
						  <span class="text">Change Password</span>
						</a>
					</li>
					
					
					
        </ul>
        <!--/ END Template Navigation/Menu -->
        <!--/ END Sidebar summary -->
     <div style="background: rgb(0, 0, 0) none repeat scroll 0% 0%; width: 8px; position: absolute; top: 0px; opacity: 0.4; display: none; border-radius: 7px; z-index: 99; right: 0px; height: 626px;" class="scrollbar"></div><div style="width: 8px; height: 100%; position: absolute; top: 0px; display: none; border-radius: 7px; background: rgb(51, 51, 51) none repeat scroll 0% 0%; opacity: 0.2; z-index: 90; right: 0px;" class="scrollrail"></div>
	</div>
      <!--/ END Sidebar Container -->
    </aside>
    <!--/ END Template Sidebar (Left) -->