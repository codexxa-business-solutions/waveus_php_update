var jq = jQuery.noConflict();

function initImage(url,view_url) {

	jq("#ImageForm").validate({
		ignore:":not(:visible)",
				rules: {
					image : {
						required:true,
					},
					order_by : {
						required:true,						
					},
				}
	});	
	
		
		jq("#ImageForm").submit(function( e ) {
	    e.preventDefault();
		var isValid = jq(this).valid();
		var formData = new FormData(jq('#ImageForm').get(0));		
		var formurl = jq('#url').val();
		var redirecturl = jq('#redirecturl').val();
			if(isValid == true){
				ajaxindicatorstart("Please wait.",view_url);
				jq.ajax({
					url: formurl,
					type: jq(this).attr("method"),
					dataType: "JSON",
					data: new FormData(this),
					processData: false,
					contentType: false,
					success: function (response)
					{
						if(response.status == 'success'){
							ajaxindicatorstop();
							window.location.href = redirecturl;
						}
						else{
							ajaxindicatorstop();
							appendMsgs('#ImageForm',response.msg,'.setError');
						}
					},
					error: function (xhr, desc, err)
					{
						ajaxindicatorstop();
						appendMsgs('#ImageForm',response.msg,'.setError');
					}
				});
			}
		});
		
}


function initeditImage(url,view_url) {

	jq("#ImageForm").validate({
		ignore:":not(:visible)",
				rules: {
				
					order_by : {
						required:true,						
					},
				}
	});	
	
		
		jq("#ImageForm").submit(function( e ) {
	    e.preventDefault();
		var isValid = jq(this).valid();
		var formData = new FormData(jq('#ImageForm').get(0));		
		var formurl = jq('#url').val();
		var redirecturl = jq('#redirecturl').val();
			if(isValid == true){
				ajaxindicatorstart("Please wait.",view_url);
				jq.ajax({
					url: formurl,
					type: jq(this).attr("method"),
					dataType: "JSON",
					data: new FormData(this),
					processData: false,
					contentType: false,
					success: function (response)
					{
						if(response.status == 'success'){
							ajaxindicatorstop();
							window.location.href = redirecturl;
						}
						else{
							ajaxindicatorstop();
							appendMsgs('#ImageForm',response.msg,'.setError');
						}
					},
					error: function (xhr, desc, err)
					{
						ajaxindicatorstop();
						appendMsgs('#ImageForm',response.msg,'.setError');
					}
				});
			}
		});
		
}