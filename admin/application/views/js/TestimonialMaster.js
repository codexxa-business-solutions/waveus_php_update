var jq = jQuery.noConflict();

function initTestimonial(url,view_url) {

	jq("#TestimonialForm").validate({
		ignore:":not(:visible)",
				rules: {
					image : {
						required:true,
					},
					testi_name : {
						required:true,						
					},
					content : {
						required:true,						
					},
				}
	});	
	
		
		jq("#TestimonialForm").submit(function( e ) {
	    e.preventDefault();
		var isValid = jq(this).valid();
		var formData = new FormData(jq('#TestimonialForm').get(0));		
		var formurl = jq('#url').val();
		var redirecturl = jq('#redirecturl').val();
			if(isValid == true){
				ajaxindicatorstart("Please wait.",view_url);
				jq.ajax({
					url: formurl,
					type: jq(this).attr("method"),
					dataType: "JSON",
					data: new FormData(this),
					processData: false,
					contentType: false,
					success: function (response)
					{
						if(response.status == 'success'){
							ajaxindicatorstop();
							window.location.href = redirecturl;
						}
						else{
							ajaxindicatorstop();
							appendMsgs('#TestimonialForm',response.msg,'.setError');
						}
					},
					error: function (xhr, desc, err)
					{
						ajaxindicatorstop();
						appendMsgs('#TestimonialForm',response.msg,'.setError');
					}
				});
			}
		});
		
}


function initeditTestimonial(url,view_url) {

	jq("#TestimonialForm").validate({
		ignore:":not(:visible)",
				rules: {
					testi_name : {
						required:true,						
					},
					content : {
						required:true,						
					},
				}
	});	
	
		
		jq("#TestimonialForm").submit(function( e ) {
	    e.preventDefault();
		var isValid = jq(this).valid();
		var formData = new FormData(jq('#TestimonialForm').get(0));		
		var formurl = jq('#url').val();
		var redirecturl = jq('#redirecturl').val();
			if(isValid == true){
				ajaxindicatorstart("Please wait.",view_url);
				jq.ajax({
					url: formurl,
					type: jq(this).attr("method"),
					dataType: "JSON",
					data: new FormData(this),
					processData: false,
					contentType: false,
					success: function (response)
					{
						if(response.status == 'success'){
							ajaxindicatorstop();
							window.location.href = redirecturl;
						}
						else{
							ajaxindicatorstop();
							appendMsgs('#TestimonialForm',response.msg,'.setError');
						}
					},
					error: function (xhr, desc, err)
					{
						ajaxindicatorstop();
						appendMsgs('#TestimonialForm',response.msg,'.setError');
					}
				});
			}
		});
		
}