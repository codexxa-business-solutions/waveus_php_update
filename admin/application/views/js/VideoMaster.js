var jq = jQuery.noConflict();

function initVideo(url,view_url) {

	jq("#VideoForm").validate({
		ignore:":not(:visible)",
				rules: {
					video_url : {
						required:true,
					},
					order_by : {
						required:true,						
					},
				}
	});	
	
		
		jq("#VideoForm").submit(function( e ) {
	    e.preventDefault();
		var isValid = jq(this).valid();
		var formData = new FormData(jq('#VideoForm').get(0));		
		var formurl = jq('#url').val();
		var redirecturl = jq('#redirecturl').val();
			if(isValid == true){
				ajaxindicatorstart("Please wait.",view_url);
				jq.ajax({
					url: formurl,
					type: jq(this).attr("method"),
					dataType: "JSON",
					data: new FormData(this),
					processData: false,
					contentType: false,
					success: function (response)
					{
						if(response.status == 'success'){
							ajaxindicatorstop();
							window.location.href = redirecturl;
						}
						else{
							ajaxindicatorstop();
							appendMsgs('#VideoForm',response.msg,'.setError');
						}
					},
					error: function (xhr, desc, err)
					{
						ajaxindicatorstop();
						appendMsgs('#VideoForm',response.msg,'.setError');
					}
				});
			}
		});
		
}
