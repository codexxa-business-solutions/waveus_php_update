var jq = jQuery.noConflict();

function getAjax(url){
  var tables = [{"table_name":"ajax_demo","tab_alias":"","join_on":"","for":""}];
  var data = {"table":tables,"index":"id"};
  var cols = [{"data": "name","orderable":false,"searchable":false},{ "data": "lname" },{ "data": "pos" }];
  makeDataTable('#example',url+'AjaxController/getData',data,cols);
}
