var jq = jQuery.noConflict();
jq(document).ready(function() {
	jq('.scrollbarsin').ClassyScroll();
	jq('.error').remove();
	// initPage();
	jq('.data_table_cls').DataTable();
});

jq('.number_only').keypress(function(e){
    if (e.which != 0 && (e.which < 48 || e.which > 57)) {
      return false;
    }
});

function validateFloatKeyPress(el, evt) {
    var charCode = (evt.which) ? evt.which : event.keyCode;
    var number = el.value.split('.');
    if (charCode != 8 && charCode != 46 && charCode > 31 && (charCode < 47 || charCode > 57)) {
        return false;
    }
	
    //just one dot
    if(number.length>1 && charCode == 46){
		if(charCode != 8)
		{
			return false;	
		}
		else
		{
			return true;
		}
    }
	else
	{
		return true;
	}
	
    //get the carat position
    var caratPos = getSelectionStart(el);
    var dotPos = el.value.indexOf(".");
    if( caratPos > dotPos && dotPos>-1 && (number[1].length > 1)){
        return false;
    }
	
    return true;
}

function getSelectionStart(o) {
	if (o.createTextRange) {
		var r = document.selection.createRange().duplicate()
		r.moveEnd('character', o.value.length)
		if (r.text == '') return o.value.length
		return o.value.lastIndexOf(r.text)
	} else return o.selectionStart
}


function ajaxindicatorstart(text,url) {
    
    if (jq('body').find('#resultLoading').attr('id') != 'resultLoading') {
        jq('body').append('<div id="resultLoading" style="display:none"><div><img src="'+url+'img/loading.gif" style="height:200px;width:300px;"><div id="divtext">' + text + '</div></div><div class="bg"></div></div>');
    }
    else {
        jq("#divtext").html(text)
    }

    jq('#resultLoading').css({
        'width': '100%',
        'height': '100%',
        'position': 'fixed',
        'z-index': '10000000',
        'top': '0',
        'left': '0',
        'right': '0',
        'bottom': '0',
        'margin': 'auto'
    });

    jq('#resultLoading .bg').css({
        'background': '#000000',
        'opacity': '0.7',
        'width': '100%',
        'height': '100%',
        'position': 'absolute',
        'top': '0'
    });

    jq('#resultLoading>div:first').css({
        'width': '100%',
        'height': '75px',
        'text-align': 'center',
        'position': 'fixed',
        'top': '0',
        'left': '0',
        'right': '0',
        'bottom': '0',
        'margin': 'auto',
        'font-size': '16px',
        'z-index': '10',
        'color': '#ffffff'

    });

    jq('#resultLoading .bg').height('100%');
    jq('#resultLoading').fadeIn(1000);
    jq('body').css('cursor', 'wait');
}


function ajaxindicatorstop() {
    jq('#resultLoading .bg').height('100%');
    jq('#resultLoading').fadeOut(500);
    jq('body').css('cursor', 'default');
}

var deleteUrl = 'UserMaster/deleteFunction';
var tab_url = 'AjaxController/getData';
//Common function for ajax call
function simpleAjax(uUrl,uType,uData,cb,cberror){
  if(uUrl != ''){
  jq.ajax({
    url : uUrl,
    type: uType,
    data: uData,
	async:false,
    success: function(response){
      if(typeof(cb) == 'function' && typeof(cb) != 'undefined'){
        cb(response);
      }
    },
    error : function(xhr,status){
      if(typeof(cberror) == 'function' && typeof(cberror) != 'undefined'){
        cberror(xhr,status);
      }
    }
  });
  }
  else{
    return false;
  }
}

//function to send mime data
function imageAjax(uUrl,uType,uData,cb,cberror){
  if(uUrl != ''){
  jq.ajax({
    url : uUrl,
    type: uType,
    data: uData,
    processData: false,
    contentType: false,
    success: function(response){
      if(typeof(cb) == 'function' && typeof(cb) != 'undefined'){
        cb(response);
      }
    },
    error : function(xhr,status){
      if(typeof(cberror) == 'function' && typeof(cberror) != 'undefined'){
        cberror(xhr,status);
      }
    }
  });
  }
  else{
    return false;
  }
}

//function to append msgs to the DOM
function appendMsgs(form,msg,errorClass){
  jq('.error').remove();
  if(typeof(msg) == 'object'){
    jq.each(msg,function(m,k){
        jq(form+' input[name="'+m+'"]').parent('div').append('<span id="'+m+'-error" class="error">'+k+'</span>');
        jq(form+' select[name="'+m+'"]').parent('div').append('<span id="'+m+'-error" class="error">'+k+'</span>');
        jq(form+' textarea[name="'+m+'"]').parent('div').append('<span id="'+m+'-error" class="error">'+k+'</span>');
    });
    }
    else{
      if(errorClass != ''){
      jq(form+' '+errorClass).html(msg);
      }
    }
}

function makeDeleteString(table,field,value){
  return 'table='+table+'&field='+field+'&value='+value; 
}

function time_picker(id)
{
	jq(id).timepicker();
}

function datePicker(id){
  jq(id).datepicker({
	defaultDate: -1, 
	minDate:'-0d',
    changeYear: true,
    changeMonth: true,
    dateFormat : 'dd-mm-yy',
  });
}


/**
* Javascript For Changepassword.
*/

jq("#ChangePasswordForm").validate({
				rules: {
					old_password : {
						required:true
						},
					new_password : {
						required:true,
						},
					re_new_password : {
						required:true,
						equalTo:'#new_password'
						},
					
				}
	});

jq("#ChangePasswordForm").submit(function( e ) {
	      e.preventDefault();
		var isValid = jq(this).valid();
		var formData = jq(this).serializeArray();
		var formurl = jq("#ChangePasswordForm #url").val();
			if(isValid == true){
			  simpleAjax(formurl,'POST',formData,function(response){
				response = JSON.parse(response);
				//if(response.status == 'success'){
					location.reload();
				// }
				// else{
				  // appendMsgs('#ChangePasswordForm',response.msg,'.setError');
				// }
			  },function(xhr,status){
				alert(status);
			  });
			}
		});
