<?php
include('includes/header.php');
include('includes/sidebar.php');
?>
	<section id="main" role="main">
      <!-- START Template Container -->
    <div class="container-fluid">  
		<!-- Track Header -->
            <!-- Toolbar -->
          <div class="col-md-12">
              <ol class="breadcrumb">
					<li><a href="<?php echo $base_url; ?>AdminMaster/dashboard">Dashboard</a></li>
					<li class="active">List Candidate(s)</li>
              </ol>
           </div>
            <!--/ Toolbar -->
        <!-- Insurance Header -->
<div class="">
          <div class="col-md-12">
            <!-- START panel -->
            <div class="panel panel-default">
              <!-- panel heading/header -->
              <div class="panel-heading genrl">
                <span>List Candidate(s)</span>
                
              </div>
                
              <!--/ panel heading/header -->
              <!-- panel body -->
              <div class="panel-body">
				 <div class="table-responsive grid-text">
				 <form action="#" method="post" id="multideleteimg">
					<table class="table table-bordered grid-table table-hover data_table_cls">
						<thead>
						  <tr>
							<th><input type="checkbox" id="select_all" value=""/></th>
							<th>Name</th>
							<th>Email</th>
							<th>Mobile</th>
							<th>Designation</th>
							<th>Year Of Experience</th>
							<th>Current Salary</th>
							<th>Willing to Relocate</th>
							<th>Notice Period in Days</th>
							<th>View Resume</th>
						  </tr>
						</thead>
						<tbody>
						<?php
					  if(@$list_candidate)
					  {
						  foreach(@$list_candidate as $val)
						  {
						  ?>
						 
							<tr>
								<td><input type="checkbox" name="checked_id[]" class="checkbox" value="<?php echo @$val->t_id; ?>"/></td>
								<td>							   
								   <?php echo $val->name; ?>
								</td>
								<td><?php echo $val->email; ?></td>
								<td><?php echo $val->mobile; ?></td>
								<td><?php echo $val->designation; ?></td>
								<td><?php echo $val->YOE; ?></td>
								<td><?php echo $val->salary; ?></td>
								<td><?php echo $val->relocate; ?></td>
								<td><?php echo $val->noticePeriod; ?></td>
								
								<td>
									<a href="<?php echo $view_url; ?><?php echo $val->resume; ?>" target="_blank">View Resume</a>
								</td>
							</tr>
						 
						 <?php
						  }
					  }
					  ?></tbody>
					</table>
					</form>
				 </div>				
              </div>
              <!-- panel body -->
            </div>
            </div>
		  </div>
	</div>
	</section>
<?php 
include('includes/footer.php');
?>