<?php
include('includes/header.php');
include('includes/sidebar.php');
?>
	<section id="main" role="main">
      <!-- START Template Container -->
    <div class="container-fluid">  
		<!-- Track Header -->
            <!-- Toolbar -->
          <div class="col-md-12">
              <ol class="breadcrumb">
					<li><a href="<?php echo $base_url; ?>AdminMaster/dashboard">Dashboard</a></li>
					<li class="active">List Career(s)</li>
              </ol>
           </div>
            <!--/ Toolbar -->
        <!-- Insurance Header -->
<div class="">
          <div class="col-md-12">
            <!-- START panel -->
            <div class="panel panel-default">
              <!-- panel heading/header -->
              <div class="panel-heading genrl">
                <span>List Career(s)</span>
                <span style="float:right"><a href="javascript:void(0)" id="multiDeleteclick"><span class="glyphicon glyphicon-trash"></span>Delete</a> &nbsp; <a href="<?php echo $this->config->item('base_url'); ?>CareerMaster/addCareer"><i class="fa fa-plus" aria-hidden="true"></i>Add Career</a></span>
              </div>
                <?php 
					if($this->session->flashdata('MSG')){
                ?>
               <div class="alert alert-success fade in">
				<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                 <?php
					echo $this->session->flashdata('MSG');
                ?>
              </div>
              <?php
                }
                 ?>
              
                 <?php 
              if($this->session->flashdata('ERROR')){
                ?>
              <div class="alert alert-danger fade in">
				<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                <?php
					echo $this->session->flashdata('ERROR');
                ?>
              </div>
              <?php 
              }
              ?>
              <!--/ panel heading/header -->
              <!-- panel body -->
              <div class="panel-body">
				 <div class="table-responsive grid-text">
				 <form action="<?php echo $base_url; ?>CareerMaster/multiDelete" method="post" id="multideleteimg">
					<table class="table table-bordered grid-table table-hover data_table_cls">
						<thead>
						  <tr>
							<th><input type="checkbox" id="select_all" value=""/></th>
							<th>Job Title</th>
							<th>Job Descriptions</th>
							<th>Desired Skills</th>
							<th>Action</th>
						  </tr>
						</thead>
						<tbody>
						<?php
					  if(@$list_career)
					  {
						  foreach(@$list_career as $val)
						  {
						  ?>
						 
							<tr>
								<td><input type="checkbox" name="checked_id[]" class="checkbox" value="<?php echo @$val->t_id; ?>"/></td>
								<td>							   
								   <?php echo $val->title; ?>
								</td>
								<td><?php echo substr($val->testi_name, 0, 100).'...'; ?></td>
								<td><?php echo substr($val->content, 0, 100).'...'; ?></td>
								
								<td>
									<a href="javascript:void(0);" data-id="<?php echo @$val->t_id; ?>" class="btn btn-danger delete_category"  type="button" data-title="Delete"><span class="glyphicon glyphicon-trash"></span></a>&nbsp/&nbsp; 
									<a href="<?php echo @$base_url; ?>CareerMaster/editCareer/<?php echo @$val->t_id; ?>" class="btn btn-danger"  type="button" data-title="Edit"><span class="glyphicon glyphicon-pencil"></span></a>
								</td>
							</tr>
						 
						 <?php
						  }
					  }
					  ?></tbody>
					</table>
					</form>
				 </div>				
              </div>
              <!-- panel body -->
            </div>
            </div>
		  </div>
	</div>
	</section>
<?php 
include('includes/footer.php');
?>
<script src="<?php echo $base_adminurl_views; ?>js/bootstrap.min.js"></script>
<script src="<?php echo $base_adminurl_views; ?>js/bootbox.min.js"></script>
<script>
$(".delete_category").click(function(){
	var id = $(this).attr("data-id");
	
	 if(id){
		  bootbox.confirm({
			message: "Are you sure you want to delete this Career?",
			buttons: {
			  confirm: {
				label: 'Yes',
				className: 'btn-success'
			  },
			  cancel: {
				label: 'No',
				className: 'btn-danger'
			  }
			},
			callback: function (result) {
			  if(result==true)
			  {
					window.location.href = '<?php echo $base_url; ?>CareerMaster/deleteCareer/'+id;
			  }
			}
		  });
		}
	});
	
	 $('#select_all').on('click',function(){
        if(this.checked){
            $('.checkbox').each(function(){
                this.checked = true;
            });
        }else{
             $('.checkbox').each(function(){
                this.checked = false;
            });
        }
    });
	
    $('.checkbox').on('click',function(){
        if($('.checkbox:checked').length == $('.checkbox').length){
            $('#select_all').prop('checked',true);
        }else{
            $('#select_all').prop('checked',false);
        }
    });
	
	$(document).on('click','#multiDeleteclick',function(){
		
		var numberOfChecked = $('input:checkbox:checked').length;
		if(numberOfChecked > 0)
		{
			
	  bootbox.confirm({
			message: "Are you sure you want to delete this Career?",
			buttons: {
			  confirm: {
				label: 'Yes',
				className: 'btn-success'
			  },
			  cancel: {
				label: 'No',
				className: 'btn-danger'
			  }
			},
			callback: function (result) {
			  if(result==true)
			  {
					document.getElementById("multideleteimg").submit(); 
			  }
			}
		  });
		  
			// bootbox.confirm("Are you sure?", function(result){
				 // document.getElementById("multideleteimg").submit(); 
			// })
		}else{
			bootbox.alert("Please check atleast one checkbox.");
		}
	
	});
	</script>