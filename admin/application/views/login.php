<?php
$front_base_url = $this->config->item('front_base_url');
$base_url 		= $this->config->item('base_url');
$http_host 		= $this->config->item('http_host');
$base_url_views = $this->config->item('base_url_views');
$base_adminurl_views = $this->config->item('base_adminurl_views');
$base_upload = $this->config->item('upload');
?> 
<!DOCTYPE #b791ff>
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<title>Waveus - Admin</title>
		<meta name="description" content="">
		<meta name="author" content="Designer_1">
		<meta name="viewport" content="width=device-width; initial-scale=1.0">
	<!-- Replace favicon.ico & apple-touch-icon.png in the root of your domain and delete these references -->
	<link rel="shortcut icon" href="/favicon.ico">
    <link rel="stylesheet" href="<?php echo $base_adminurl_views; ?>/css/bootstrap.css">
    <link rel="stylesheet" href="<?php echo $base_adminurl_views; ?>/css/font-awesome.css">
    <link rel="stylesheet" href="<?php echo $base_adminurl_views; ?>/css/animate.css">
    <link type="text/css" href="<?php echo $base_adminurl_views; ?>/css/style.css" rel="stylesheet">
	</head>
	<body>
	<div class="container">
	<div class="full-login">
	<div class="row">
		<div class="col-6 col-sm-6 col-lg-4 col-lg-offset-4 col-sm-offset-3 col-md-offset-3">
<!--           data-wow-delay="0.9s" animation-delay:"0.9s"-->
			<div class="loginLogo animated wow bounceInDown" data-wow-delay="0.9s" animation-delay="0.9s">
				<img alt="" style="width:220px;height:105px;" src="<?php echo $base_adminurl_views; ?>/img/logo.png">
			</div>
<!--          data-wow-delay="0.5s" animation-delay:"0.5s" fadeInDown-->
			<div class="sighin animated wow fadeInDown" data-wow-delay="0.5s" animation-delay="0.5s">
				<h1>Sign in</h1>
				<form action="<?php echo $base_url;?>AdminMaster/login" id="login_form" method="post" class="form-horizontal">
				<?php
				if(@$L_strErrorMessage)
				{
				?>
				<div class="alert alert-danger fade in">
						<?php echo @$L_strErrorMessage; ?>
				</div>
				<?php
				}
				?>
					<INPUT TYPE="hidden" NAME="hidPgRefRan" VALUE="<?php echo rand();?>">
					<INPUT TYPE="hidden" NAME="action" VALUE="login">
									<div class="form-group">
										<div class="col-12 col-sm-12 col-lg-12 login-box">
											<div class="input-icon">
												<i class="fa fa-user"></i>
											<input type="text" id="username" placeholder="Enter your username" name="username" class="form-control" maxlength="100" value="<?php if(get_cookie('remember_me')=='yes'){ echo get_cookie('username'); } ?>" aria-required="true">
                                              
										</div>
										</div>
									</div>
									<div class="form-group">
										<div class="col-12 col-sm-12 col-lg-12 login-box">
											<div class="input-icon">
											<i class="fa fa-lock"></i>
											<input type="password" id="password" value="<?php if(get_cookie('remember_me')=='yes'){ echo get_cookie('password'); } ?>" placeholder="Enter your password" name="password" class="form-control" maxlength="100"  aria-required="true">
											</div>
										</div>
									</div>
								<div class="form-group">
									<div class="col-md-12">
										<div class="checkbox">
											<label>
												<input id="remember" name="remember_me" value="yes" id="remember_me" type="checkbox" <?php if(get_cookie('remember_me')=='yes'){ echo "checked"; } ?> /> Remember Me
											</label>
										</div>
									</div>
								</div>
								<div class="form-group">
									<div class="col-md-12">
										<input type="submit" id="login_btn"  class="btn" value="Submit">
									</div>
								</div>
							</form>
			</div>
		</div>
</div>
</div>
</div>
<div class="footer-login">Copyright &copy; Codexxa Business Solution. All Rights reserved. 
	<script type="text/javascript" src="<?php echo $base_adminurl_views; ?>/js/jquery-2.1.4.min.js"></script>
	<script type="text/javascript" src="<?php echo $base_adminurl_views; ?>/js/jquery.validate.js"></script>
    <script src="<?php echo $base_adminurl_views; ?>/js/bootstrap.js"></script>
    <script type="text/javascript" src="<?php echo $base_adminurl_views; ?>/js/wow.js"></script>
        <script>
            wow = new WOW(
                {
                    animateClass: 'animated',
                    offset: 0
                }
            );
            wow.init();
			$("#login_form").validate({
			rules: {
				username : {
					required:true
					},
				password : {
					required:true
					},
		messages: {
			username :{
						required:"<font color='red'>username is required.</font>"
					},
			password :{
						required:"<font color='red'>password is required.</font>"
					},
			},
		}
	});
</script>
</body>
</#b791ff>