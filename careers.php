<!doctype html>
<html lang="en" class="no-js">
<head>
	<title>Careers</title>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<style>
		.page-banner h2 {
    float: none!important;
}
	</style>
	<?php include("header.php"); include('config.php'); ?>
	<?php
  
  $errors = array();
  
  if($_SERVER['REQUEST_METHOD'] == 'POST'){
  
  if(preg_match("/\S+/", $_POST['name']) === 0){
    $errors['name'] = "*First Name is required.";
  }
  if(preg_match("/\S+/", $_POST['designation']) === 0){
    $errors['designation'] = "* Designation is required.";
  }
  if(preg_match("/\S+/", $_POST['relocate']) === 0){
    $errors['relocate'] = "* Relocate is required.";
  }
  if(preg_match("/.+@.+\..+/", $_POST['email']) === 0){
    $errors['email'] = "* Not a valid e-mail address.";
  }
   if(preg_match("/^[0-9]{10}+$/", $_POST['mobile']) === 0){
    $errors['mobile'] = "* Mobile no must contain 10 digits.";
  }
    if(preg_match("/^[0-9]{2}+$/", $_POST['YOE']) === 0){
    $errors['YOE'] = "* Experience must contain 2 digits.";
  }
    if(preg_match("/^[0-9]{0}+$/", $_POST['salary']) === 0){
    $errors['salary'] = "* Salary must required.";
  }
  if(preg_match("/.{2,}/", $_POST['noticePeriod']) === 0){
    $errors['noticePeriod'] = "*Notice period must required.";
  }
  if(strcmp($_POST['password'], $_POST['cpassword'])){
    $errors['cpassword'] = "* Password do not much.";
  }
  
  }
?>

		<!-- content 
			================================================== -->
		<div id="content">

					 <!-- slider 
			================================================== -->
			<div id="slider">
			<div class="flexslider">
				<ul class="slides">
					<li>
						<img alt="" src="upload/careers-banner.jpg" />
					</li>
				</ul>
		    </div>
		</div>
		<!-- End slider -->

			<div class="fullwidth-box" style="margin-bottom:5rem">
				<div class="container">
				
					<!-- faqs-box -->
					<div class="faqs-box">
						<div class="row">
						 

							<div class="col-md-12 col-lg-12 col-sm-12">
								<h3>Current Opening</h3>
								<div class="accordion-box">
								
								<?php
									$sql = "SELECT * FROM career_master";
									$result = $conn->query($sql);
									$i = 1;
										if ($result->num_rows > 0) {
											// output data of each row
											while($row = $result->fetch_assoc()) {
									
								?>
								
								
									<div class="accord-elem <?php if($i == 1){ ?>active<?php } ?>">
										<div class="accord-title">
											<h4><?php echo $row["title"]; ?></h4>
											<a class="accord-link" href="#"></a>
										</div>
										<div class="accord-content" <?php if($i == 1){ ?>style="display: block;<?php } ?>">
											<h3>Job Descriptions:</h3>
											<p><?php echo $row["testi_name"]; ?></p>
											<h3>Desired Skills:</h3>
											<p><?php echo $row["content"]; ?></p>
											<a class="main-button" href="#" style="border:1px solid #0076f9" data-toggle="modal" data-target="#myModal">Apply</a>
										</div>
									</div>

								<?php $i++; } } ?> 
								
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		 
		</div>
		<!-- End content -->
		<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
		<h5>Apply For A Job</h5><br>
        <h3 class="modal-title">Career</h3>
      </div>
      <div class="modal-body">
	  
	  
	  <form action="insert.php" id="contactForm" class="contact-form" method="post" enctype="multipart/form-data">
            <div class="row">
              <div class="col-lg-6 col-md-12">
                <div class="form-group">
                  <label>Name</label>
                  <input type="text" name="name" id="name" class="form-control" required="" data-error="Please enter your name">
                  <div class="help-block with-errors"></div>
                </div>
              </div>
              <div class="col-lg-6 col-md-12">
                <div class="form-group">
                  <label>Email Address</label>
                  <input type="email" name="email" id="email" class="form-control" required="" data-error="Please enter your email">
                  <div class="help-block with-errors"></div>
                </div>
              </div>
              <div class="col-lg-6 col-md-12">
                <div class="form-group">
                  <label>Mobile</label>
                  <input type="text" name="mobile" id="mobile" class="form-control" required="" data-error="Please enter your subject">
                  <div class="help-block with-errors"></div>
                </div>
              </div>
              <div class="col-lg-6 col-md-12">
                <div class="form-group">
                  <label>Designation</label>
                  <input type="text" name="designation" id="designation" class="form-control" required="" data-error="Please Enter Designation">
                  <div class="help-block with-errors"></div>
                </div>
              </div>
              <div class="col-lg-6 col-md-6">
                <div class="form-group">
                  <label>Year Of Experience</label>
                  <input type="number" name="YOE" id="YOE" class="form-control" required="" data-error="Year Of Experience">
                  <div class="help-block with-errors"></div>
                </div>
              </div>
			  <div class="col-lg-6 col-md-6">
                <div class="form-group">
                  <label>Current Salary</label>
                  <input type="number" name="salary" id="salary" class="form-control" required="" data-error="0000 INR">
                  <div class="help-block with-errors"></div>
                </div>
              </div>
			  <div class="col-lg-6 col-md-6">
                <div class="form-group">
                  <label>Willing to Relocate</label>
                  <input type="text" name="relocate" id="relocate" class="form-control" required="" data-error="Willing to Relocate">
                  <div class="help-block with-errors"></div>
                </div>
              </div>
			  <div class="col-lg-6 col-md-6">
                <div class="form-group">
                  <label>Notice Period in Days</label>
                  <input type="number" name="noticePeriod" id="noticePeriod" class="form-control" required="" data-error="No. of Days">
                  <div class="help-block with-errors"></div>
                </div>
              </div>
			  <div class="col-lg-6 col-md-6">
                <div class="form-group">
                  <label>Resume</label>
                  <input type="file" id="resume" name="resume" required="">
                  <div class="help-block with-errors"></div>
                </div>
              </div>

			  <div class="col-lg-6 col-md-6">
                <div class="form-group">
				<input type="submit" name="submit" class="btn btn-success main-button" value="Send Message">
                </div>
              </div>
              <div class="col-lg-12 col-md-12">
              </div>
            </div>
          </form>
		  
		  
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>
<?php include("footer.php");?>
		
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script type="text/javascript">
    $(document).ready(function(){
    $cf = $('#mobile');
    $cf.blur(function(e){
        phone = $(this).val();
        phone = phone.replace(/[^0-9]/g,'');
        if (phone.length != 10)
        {
            alert('Mobile number must be 10 digits.');
            $('#mobile').val('');
            // $('#mobile').focus();
        }
    });
});
// $("#contactForm").submit(function( e ) {
// 	    e.preventDefault();
// 		var formData = new FormData($('#contactForm').get(0));
// 				$.ajax({
// 					url: 'insert.php',
// 					type: $(this).attr("method"),
// 					dataType: "JSON",
// 					data: new FormData(this),
// 					processData: false,
// 					contentType: false,
// 					success: function (response)
// 					{
// 						if(response.status == 'success'){
// 							swal('Good job!', 'Your Form is submitted successfully.', 'success');
// 						}						
// 					},
// 					error: function (xhr, desc, err)
// 					{
						
// 					}
// 				});
// 		});
</script>
	</div>
	<!-- End Container -->

</body>

</html>

<?php
// if (isset($_FILES) && (bool) $_FILES) {
// 	$name=$_POST['name'];
// 		$email=$_POST['email'];
// 		$mobile=$_POST['mobile'];
// 		$YOE=$_POST['YOE'];
// 		$salary=$_POST['salary'];
// 		$relocate=$_POST['relocate'];
// 		$noticePeriod=$_POST['noticePeriod'];

//     $allowedExtensions = array("pdf", "doc", "docx", "gif", "jpeg", "jpg", "png");

//     $files = array();
//     foreach ($_FILES as $name => $file) {
//         $file_name = $file['name'];
//         $temp_name = $file['tmp_name'];
//         $file_type = $file['type'];
//         $path_parts = pathinfo($file_name);
//         $ext = $path_parts['extension'];
//         if (!in_array($ext, $allowedExtensions)) {
//             die("File $file_name has the extensions $ext which is not allowed");
//         }
//         array_push($files, $file);
//     }

    // email fields: to, from, subject, and so on
    // $to ="contactus@waveus.com" ;
    // $from = $_POST['email'];
    // $subject='Job Application From Waveus career';
	// $message= "Name :".$name."\n"."Email :".$email."\n"."Mobile NO :".$mobile."\n"."YOE :".$YOE."\n"."salary :".$salary."\n"."relocate :".$relocate."\n"."noticePeriod :".$noticePeriod.;
    // $headers = "From: $from";

    // boundary 
    // $semi_rand = md5(time());
    // $mime_boundary = "==Multipart_Boundary_x{$semi_rand}x";

    // headers for attachment 
    // $headers .= "\nMIME-Version: 1.0\n" . "Content-Type: multipart/mixed;\n" . " boundary=\"{$mime_boundary}\"";

    // multipart boundary 
    // $message = "This is a multi-part message in MIME format.\n\n" . "--{$mime_boundary}\n" . "Content-Type: text/plain; charset=\"iso-8859-1\"\n" . "Content-Transfer-Encoding: 7bit\n\n" . $message . "\n\n";
    // $message .= "--{$mime_boundary}\n";

    // preparing attachments
    // for ($x = 0; $x < count($files); $x++) {
    //     $file = fopen($files[$x]['tmp_name'], "rb");
    //     $data = fread($file, filesize($files[$x]['tmp_name']));
    //     fclose($file);
    //     $data = chunk_split(base64_encode($data));
    //     $name = $files[$x]['name'];
    //     $message .= "Content-Type: {\"application/octet-stream\"};\n" . " name=\"$name\"\n" .
    //             "Content-Disposition: attachment;\n" . " filename=\"$name\"\n" .
    //             "Content-Transfer-Encoding: base64\n\n" . $data . "\n\n";
    //     $message .= "--{$mime_boundary}\n";
    // }
    // send

//     $ok = mail($to, $subject, $message, $headers);
//     if ($ok) {
// 		echo "<script>alert('Sent Successfully! Thank you, We will contact you shortly!')</script>";
//     } else {
//         echo "<script>alert('Failed');</script>";
//     }
// }
?>

