<!doctype html>
<html lang="en" class="no-js">
<head>
	<title>Software Engineering| Contact Us| Waveus Networks Pvt Ltd</title>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<meta name="keywords" content="4G, 5G, MAC, PHY, Platform, Virtualization, Cloud, Orchestration, Telecom IP, EdTech Software, Vehicle Tracking, Industrial IoT" />
	<meta name="Description" content="If you require software engineering solutions or products to overcome business challenges that may be holding you back from achieving growth and realizing success, then contact us today. We can help.">

	 <?php include("header.php");?>

		<!-- content 
			================================================== -->
		<div id="content">

				 <!-- slider 
			================================================== -->
			<div id="slider">
			<div class="flexslider">
				<ul class="slides">
					<li>
						<img alt="" src="upload/contact.jpg" />
					</li>
				</ul>
		    </div>
		</div>
		<!-- End slider -->

			<section class="section faqs-box" style="text-align:center">
				<div class="container">
					<div class="contact-heading">
						<h1>We Love to Hear From You.</h1>
					</div>
					<div>
						<p>We strive for excellence by delivering innovative and cutting-edge technologies and software engineering solutions. For companies in the healthcare, telecom, or any other industry struggling to cope with unique industry challenges and looking for solutions that lead to business transformation and better connectivity, look no further. </p>
					</div>

					<div class="contact-heading2">
						<h2>Contact us for help.</h2>
						<p>We leverage the power of the latest and emerging technologies like 4G, 5G, industrial IoT, and others to provide you with solutions that deliver results.</p>
					</div>
					<div class="contact-heading2">
						<h2>Call on +91-9677541110.</h2>
						<span>Or</span>
						<br>
						<br>
						<p>Fill the form given below with relevant information. Our representative will get back to you right away.</p>
					</div>
				</div>
			</section>
			<!-- contact box -->
			<div class="contact-box" style='padding:3rem 0'>
				<div class="container">
					<div class="row">

						<div class="col-md-3">
							<div class="contact-information">
								<h3>Contact info</h3>
								<ul class="contact-information-list">
									<li><span><i class="fa fa-home"></i>B204, Mana Candela, Off Sarjapur Rd, Electronic City Phase II, Kodathi, Bengaluru, Karnataka 560035</span></li>
									<li><span><i class="fa fa-phone"></i>+91-9677541110</span></li>
									<!-- <li><a href="#"><i class="fa fa-envelope"></i>contactus@waveus.com</a></li> -->
								</ul>
							</div>
						</div>
						<div class="col-md-9">
							<h3>Send us a message</h3>
							<form id="contact-form" action="" method="post">

								<div class="text-input">
									<div class="float-input">
										<input name="name" id="name" type="text" placeholder="Name">
										<span><i class="fa fa-user"></i></span>
									</div>

									<div class="float-input2">
										<input name="mail" id="mail" type="text" placeholder="Email">
										<span><i class="fa fa-envelope"></i></span>
									</div>
								</div>

								<div class="textarea-input">
									<textarea name="message" id="message" placeholder="Message"></textarea>
									<span><i class="fa fa-comment"></i></span>
								</div>
								<div class="g-recaptcha brochure__form__captcha" data-sitekey="6LfJ2WwaAAAAAP8gUnhhp07gpppd6dx7X2Be-1pm"></div>
								<div id="msg" class="message"></div>
								<input type="submit" name="submit" id="submit_contact" value="Send Message">

							</form>
						</div>

					</div>
				</div>
			</div>

		</div>
		<!-- End content -->
		<script src="https://www.google.com/recaptcha/api.js"></script>
<script>

function get_action(form) 
{
    var v = grecaptcha.getResponse();
    if(v.length == 0)
    {
        document.getElementById('captcha').innerHTML="You can't leave Captcha Code empty";
        return false;
    }
    else
    {
         document.getElementById('captcha').innerHTML="Captcha completed";
        return true; 
    }
}

</script>
<?php include("footer.php");?>

	</div>
	 
</body>
 
</html>
<?php 
if(isset($_POST['submit'])){
	$name=$_POST['name'];
		$email=$_POST['email'];
		$message=$_POST['message'];

	
	// $to='contactus@waveus.com';
	$to='vkwkahare@gmail.com';
	// Receiver Email ID, Replace with your email ID
	$subject='Waveus Form Submission';
	$message="Name :".$name."\n"."Email :".$email."\n"."Message :".$message;
	$headers="From: ".$email;

	if(mail($to, $subject, $message, $headers)){
		echo "<script>alert('Sent Successfully! Thank you, We will contact you shortly!')</script>";
	}
	else{
		echo "<script>alert('Failed');</script>";
	}
	}

	$recaptcha = $_POST['g-recaptcha-response'];
$res = reCaptcha($recaptcha);
if(!$res['success']){
  alert("failed");
}

	function reCaptcha($recaptcha){
		$secret = "6LfJ2WwaAAAAAAZ5dosRwOqydvv5KdjrHdfzrOhX";
		$ip = $_SERVER['REMOTE_ADDR'];
	  
		$postvars = array("secret"=>$secret, "response"=>$recaptcha, "remoteip"=>$ip);
		$url = "https://www.google.com/recaptcha/api/siteverify";
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_TIMEOUT, 10);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $postvars);
		$data = curl_exec($ch);
		curl_close($ch);
	  
		return json_decode($data, true);
	  }
	?>

	<!-- site key - 6LfJ2WwaAAAAAP8gUnhhp07gpppd6dx7X2Be-1pm -->
	<!-- secrete key - 6LfJ2WwaAAAAAAZ5dosRwOqydvv5KdjrHdfzrOhX -->