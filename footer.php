<!-- footer 
			================================================== -->
            <footer>
			<div class="up-footer">
				<div class="container">
					<div class="row">

						<div class="col-md-3">
							<div class="widget footer-widgets twitter-widget">
								<h1><a href="#"><img alt=""width="150px" style="background-color:#fff;padding:5px" src="images\waveusLogo.png"></a></h1>
								 <p style="padding:1rem 0; color:#fff;text-align:justify">we are a company invested in research and development and strongly committed to developing and engineering next-generation technologies and software systems that help clients increase their business potentials while navigating through complexities smoothly with greater success.</p>
							</div>
						</div>

						<div class="col-md-3">
							<div class="widget footer-widgets tag-widget">
								<h4>Quick Links</h4>
								<ul class="tag-widget-list">
									<li><a href="about.php">About Us</a></li>
									<li><a href="contact.php">Contact Us</a></li>
									<li><a href="careers.php">Careers</a></li>
									<li><a href="news-events.php">News & Events</a></li>
									 
								</ul>
							</div>
						</div>

						<div class="col-md-3">
							<div class="widget footer-widgets info-widget">
								<h4>Contact Us</h4>
								<ul class="contact-list">
									<li><a class="phone" href="#"><i class="fa fa-phone"></i><span>+91-9677541110</span></a></li>
									<!-- <li><a class="mail" href="#"><i class="fa fa-envelope"></i><span>contactus@waveus.com</span></a></li> -->
									<li><a class="address" href="#"><i class="fa fa-home"></i><span>Bengaluru, Karnataka 560035</span></a></li>
								</ul>
							</div>
                        </div>
                        <div class="col-md-3">
							<div class="widget footer-widgets flickr-widget">
								<h4>Map</h4>
								<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d7778.494946576727!2d77.7100770427863!3d12.891802295126263!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x7732f4b4ea99d84e!2sMana%20Candela!5e0!3m2!1sen!2sin!4v1614247684506!5m2!1sen!2sin" width="100%"  style="border:0;" allowfullscreen="" loading="lazy"></iframe>
							</div>
						</div>

					</div>
				</div>
			</div>

			<div class="footer-line">
				<div class="container">
					<div class="footer-line-in">
						<div class="row">
							<div class="col-md-8">
								<p>&#169; Copyright 2021, Waveus Network Pvt Ltd</p>
							</div>
							<div class="col-md-4">
								<ul class="social-icons">
									<li><a class="facebook" href="https://www.facebook.com/WaveusNetworks"><i class="fa fa-facebook"></i></a></li>
									<!-- <li><a class="twitter" href="#"><i class="fa fa-twitter"></i></a></li> -->
									<!-- <li><a class="rss" href="#"><i class="fa fa-rss"></i></a></li> -->
									<!-- <li><a class="google" href="#"><i class="fa fa-google-plus"></i></a></li> -->
									<li><a class="linkedin" href="https://in.linkedin.com/company/waveus-networks"><i class="fa fa-linkedin"></i></a></li>
									<!-- <li><a class="pinterest" href="#"><i class="fa fa-pinterest"></i></a></li> -->
								</ul>
							</div>
						</div>						
					</div>
				</div>
			</div>

		</footer>
		<!-- End footer -->
		<div id="cookie" class="cc-window cc-banner cc-type-info cc-theme-block cc-bottom cookie-alert no-print">
			<div class="box-cookies">
			<span id="cookieconsent:desc" class="cc-message">
			(We use cookies and related Tracking & Analytics technologies to personalize and enhance your experience on our site. Visit our Cookie Policy to learn more about cookies and how to manage your personal preferences. By clicking 'Accept,' you agree to the use of cookies and related analytics & tracking technologies.) <a href="#">Read More</a></span>
			<span onclick="disabled()"  class="cc-close cc-cookie-accept-js">Accept Cookies</span>
			</div>

		</div>
		<div class="fixed-link-top">
			<div class="container">
				<a class="go-top" href="#"><i class="fa fa-angle-up"></i></a>
			</div>
        </div>
        <script>
		if ("coockie" in localStorage) {
			document.getElementById('cookie').style.display='none';
} else {
    document.getElementById('cookie').style.display='block';
}
			function disabled(){
				window.localStorage.setItem("coockie", 1);
		        document.getElementById('cookie').style.display='none';
		    }
		</script>
<script type="text/javascript" src="js/jquery.min.js"></script>
<script type="text/javascript" src="js/jquery.migrate.js"></script>
<script type="text/javascript" src="js/jquery.magnific-popup.min.js"></script>
<script type="text/javascript" src="js/bootstrap.js"></script>
<script type="text/javascript" src="js/raphael-min.js"></script>
<script type="text/javascript" src="js/DevSolutionSkill.min.js"></script>
<script type="text/javascript" src="js/jquery.bxslider.min.js"></script>
<script type="text/javascript" src="js/plugins-scroll.js"></script>
<script type="text/javascript" src="js/jquery.flexslider.js"></script>


 <!-- jQuery KenBurn Slider  -->
<script type="text/javascript" src="js/jquery.themepunch.revolution.min.js"></script>
<script type="text/javascript" src="js/script.js"></script>