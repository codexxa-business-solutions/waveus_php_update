<link rel="shortcut icon" type="image/x-icon" href="images/favicon.ico">
	<link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,400,700,600,300' rel='stylesheet' type='text/css'>

<link rel="stylesheet" href="css/bootstrap.css" type="text/css" media="screen">
<!-- REVOLUTION BANNER CSS SETTINGS -->
<link rel="stylesheet" type="text/css" href="css/fullwidth.css" media="screen"/>
<link rel="stylesheet" type="text/css" href="css/settings.css" media="screen"/>

<link rel="stylesheet" type="text/css" href="css/magnific-popup.css" media="screen">
<link rel="stylesheet" type="text/css" href="css/font-awesome.css" media="screen">
<link rel="stylesheet" type="text/css" href="css/jquery.bxslider.css" media="screen">
<link rel="stylesheet" type="text/css" href="css/style.css" media="screen">
<link rel="stylesheet" type="text/css" href="css/responsive.css" media="screen">
<link rel="stylesheet" type="text/css" href="css/flexslider.css" media="screen">

</head>
<body>

<!-- Container -->
<div id="container">
    <!-- Header
        ================================================== -->
    <header class="clearfix">
        <!-- Static navbar -->
        <div class="navbar navbar-default navbar-fixed-top">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="index.php"><img alt="" width="200px" src="images\waveusLogo.png"></a>
                </div>
                <div class="navbar-collapse collapse">
                    <ul class="nav navbar-nav navbar-right">
                        <li ><a class="active" href="index.php">Home</a>
                        </li>
                        <li class="drop"><a href="index.php">Company</a>
                            <ul class="drop-down">
                                <li><a href="about.php">About Us</a></li>
                                <li><a href="careers.php">Careers</a></li>
                                <li><a href="news-events.php">News & Events</a></li>
                            </ul>
                        </li>
                        <li class="drop"><a href="#">Solutions</a>
                            <ul class="drop-down">
                                <li><a href="Waveus-Analytica.php">Waveus Analytica</a></li>
                                <li><a href="Waveus-solution.php">Waveus Solution</a></li>
                                <!-- <li><a href="solution.php">Solution</a></li> -->
                            </ul>
                        </li>
                        <li class="drop"><a href="#">Products</a>
                            <ul class="drop-down">
                                <li><a href="nFiniteSkills.php">nFiniteSkills - Learning From Future</a></li>
                                <li><a href="4Ci-20-Content.php">4Ci - Content, Caching, Computing, Communication</a></li>
                            </ul>
                        </li>
                        <li><a href="contact.php">Support Center</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </header>
    <!-- End Header -->