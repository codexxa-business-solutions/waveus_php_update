<?php
    if(isset($_GET['success_msg']) && $_GET['success_msg']==1){
    echo "<script>alert('Your Application has been send Successfully')</script>";}


?>
<!doctype html>
<html lang="en" class="no-js">
<head>
	<title>Waveus Network Pvt Ltd</title>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

	<?php include("header.php");?>

		<!-- slider 
			================================================== -->
		<div id="slider">
			<div class="fullwidthbanner-container">
				<div class="fullwidthbanner">
					<ul>
						<!-- THE FIRST SLIDE -->
						<li data-transition="3dcurtain-vertical" data-slotamount="10" data-masterspeed="300">
						 <!-- THE MAIN IMAGE IN THE FIRST SLIDE -->
							<img alt="" src="upload/slider-revolution/1 copy.jpg" >
								<!-- THE CAPTIONS IN THIS SLDIE -->
								<div class="caption large_text sfb"
									data-x="15"
									data-y="100"
									data-speed="600"
									data-start="1200"
									data-easing="easeOutExpo" data-end="7000" data-endspeed="300" data-endeasing="easeInSine" >Niche Solutions</div>

								<div class="caption big_white sft stt"
									data-x="15"
									data-y="147"
									data-speed="500"
									data-start="1400"
									data-easing="easeOutExpo" data-end="7100" data-endspeed="300" data-endeasing="easeInSine" >2013 - Right Connectivity Begins,<br><span style="color:#0076f9">2015 Connecting Machines - Machines,</span><br>2018 Connecting Humans - Machines,<br>202x - Connect Everything
									<br>
									<br>
									<a href="Waveus-solution.php" class="btn btn-primary">Read More</a>
								</div>
								
  
						</li>
						<!-- THE Second SLIDE -->
						<li data-transition="papercut" data-slotamount="15" data-masterspeed="300">
						 <!-- THE MAIN IMAGE IN THE second SLIDE -->
							<img alt="" src="upload/slider-revolution/2.jpg" >
							<!-- THE CAPTIONS IN THIS SLDIE -->
							<div class="caption big_white sft stt"
								 data-x="295"
								 data-y="116"
								 data-speed="500"
								 data-start="1200"
								 data-easing="easeOutExpo" data-end="7100" data-endspeed="300" data-endeasing="easeInSine" ><span class="opt-style">Waveus Analytica</span></div>

							<div class="caption modern_medium_fat sft stt"
								 data-x="437"
								 data-y="140"
								 data-speed="500"
								 data-start="1600"
								 data-easing="easeOutExpo" data-end="7200" data-endspeed="300" data-endeasing="easeInSine" >
								 <span class="opt-style2">Connect us to Know more...!</span>
								 <br>
									<a href="Waveus-Analytica.php" class="btn btn-primary">Read More</a>
					 
							 
						</li>

						<!-- THE third SLIDE -->
						<li data-transition="turnoff" data-slotamount="10" data-masterspeed="300">
						 <!-- THE MAIN IMAGE IN THE second SLIDE -->
							<img alt="" src="upload/slider-revolution/3.jpg" >
							<!-- THE CAPTIONS IN THIS SLDIE -->
							<div class="caption big_white sft stt slide3-style"
								 data-x="520"
								 data-y="194"
								 data-speed="500"
								 data-start="1500"
								 data-easing="easeOutExpo" data-end="7300" data-endspeed="300" data-endeasing="easeInSine" >    <span>nFiniteSkills</span>  </div>

							<div class="caption modern_medium_fat sft stt slide3-style"
								 data-x="520"
								 data-y="220"
								 data-speed="500"
								 data-start="1700"
								 data-easing="easeOutExpo" data-end="7500" data-endspeed="300" data-endeasing="easeInSine" ><span class="opt-style2">Interactive Learning Connects </span> Learning from Future
								 <br>
									<a href="nFiniteSkills.php" class="btn btn-primary">Read More</a></div>
						 
						</li>
						<!-- THE fourth SLIDE -->
						<li data-transition="turnoff" data-slotamount="10" data-masterspeed="300">
					 
						<!-- THE MAIN IMAGE IN THE second SLIDE -->
							<img alt="" src="upload/slider-revolution/4.jpg" >

							<!-- THE CAPTIONS IN THIS SLDIE -->

							<div class="caption randomrotate"
								 data-x="100"
								 data-y="40"
								 data-speed="600"
								 data-start="1200"
								 data-easing="easeOutExpo" data-end="7100" data-endspeed="300" data-endeasing="easeInSine" > 

							<div class="caption big_white sft stt slide3-style"
								 data-x="450"
								 data-y="50"
								 data-speed="500"
								 data-start="1500"
								 data-easing="easeOutExpo" data-end="7300" data-endspeed="300" data-endeasing="easeInSine" >4Ci - Content, Caching,<br><span>Computing, Communication</span></div>

							<div class="caption modern_medium_fat sft stt slide3-style"
								 data-x="450"
								 data-y="110"
								 data-speed="500"
								 data-start="1700"
								 data-easing="easeOutExpo" data-end="7500" data-endspeed="300" data-endeasing="easeInSine" ><span class="opt-style2">Edge Computing or Cloud Computing - <br></span> What makes Your Business Smarter?<br>Call Us for Free Analysis. 
								 <br>
								 <br>
									<a href="4Ci-20-Content.php" class="btn btn-primary">Read More</a></div>
							 
						</li>
					</ul>
				</div>
			</div>
		</div>
		<!-- End slider -->

		<!-- content 
			================================================== -->
		<div id="content">

			<div class="fullwidth-box">
				<div class="container">
				
					<!-- services-box -->
					<div class="services-box">
						<div class="row">
							<div class="col-md-3">
								<div class="services-post">
									<a class="services-icon1" href="#"><i class="fa fa-cogs"></i></a>
									<div class="services-post-content">
										<h4>Who We Are</h4>
										<p>We the Niche team started the vision of Niche Transform 2020. We joined hands together, to transform the way of life runs by Niche innovation by year 2020.</p>
									</div>
								</div>
							</div>

							<div class="col-md-3">
								<div class="services-post">
									<a class="services-icon2" href="#"><i class="fa fa-desktop"></i></a>
									<div class="services-post-content">
										<h4>Our Vision & Mission</h4>
										<p>Our vision is, Transform human life through referrals. Our Mission is, Innovate cutting-edge transformations for simpler human life.</p>
									</div>
								</div>
							</div>

							<div class="col-md-3">
								<div class="services-post">
									<a class="services-icon3" href="#"><i class="fa fa-book"></i></a>
									<div class="services-post-content">
										<h4>Our Products</h4>
										<p>Our unique Product portfolio is focused to deliver right solutions off the self to meet the growing customer requirements.</p>
									</div>
								</div>
							</div>

							<div class="col-md-3">
								<div class="services-post">
									<a class="services-icon3" href="#"><i class="fa fa-book"></i></a>
									<div class="services-post-content">
										<h4>Our Technologies</h4>
										<p>SMAC the emerging technology integrates Social, Mobile, Analytic and Cloud solutions for business competiveness.</p>
									</div>
								</div>
							</div>
						</div>
					</div>

				<!-- section 2 -->
				<section id="feature" class="section2 space">
						<div class="container">
							<div class="feature-heading">
								<h5>WAVEUS</h5>
								<h3>WAVEUS WE PROVIDE NICHE SOLUTION FOR YOUR BUSINESS NEEDS.</h3>
								<p>Our Solutions can accelerate your business for exponential growth.</p>
								<p>Exclusive Business Refferal Networknichehands.com /nFiniteSkills.com - Predictive Education Solution,NB-IoT - Interoperable Virtualized Platform to Application Solution /3Ci-Commerce - The disruptive i-Commerce solution powers the commerce in offline</p>
							</div>
							<div class="row pad3rem">
								<div class="col-sm-5 feature-block">
									<img src="images\resource-page-webinar-accelerate-your-business-growth-530x375_umhh7c.png" class="img-responsive" alt="Primax Phone">
								</div>
								<div class="col-sm-7 feature-block no-padding">
								<div class="row">
									<a href="#"><div class="col-sm-6 feature-item text-center">
										 
										<h5>Nichehands.com</h5>
										<p>The Business Referral Network</p>
									</div>
									</a>
									<a href="http://www.nfiniteskills.com/#/pages/index"><div class="col-sm-6 feature-item text-center">
						 
										<h5>nFiniteSkills </h5>
										<p>Infinite Skills Delivered in Finite Form</p>
									</div></a> 
									<a href="#"><div class="col-sm-6 feature-item text-center">
						 
										<h5>NB-IoT</h5>
										<p>Interoperable Virtualized Platform to Application Solution</p>
									</div></a> 
									<a href="#"><div class="col-sm-6 feature-item text-center">
						 
										<h5>3Ci-Commerce</h5>
										<p>The disruptive i-Commerce solution powers the commerce in offline</p>
									</div></a> 
								</div>
								</div>
							</div>
						</div>
					</section>
				 
				</div>
			</div>

			<!-- counter -->
			<div class="fullwidth-box">
				<div class="container"> 
					<div class="wrapper">
						<div class="col-md-12 col-lg-12">
							<div class='row'>
								<div class="col-md-3 col-lg-3 col-sm-6">
									<div class="counter col_third">
										<img src="images\icons\idea.png" width="60px" alt="">
										<div>
										<span class="timer count-title count-number" data-to="25" data-speed="1500"></span><span style="font-size:4rem">+</span>
										</div>
										<p class="count-text ">Patents</p>
									</div>
								</div>
								<div class="col-md-3 col-lg-3 col-sm-6">
									<div class="counter col_third">
										<img src="images\icons\transformation.png" width="60px" alt="">
										<div>
										<span class="timer count-title count-number" data-to="3200" data-speed="1500"></span><span style="font-size:4rem">+</span>
										</div>
										<p class="count-text ">Customers</p>
									</div>
								</div>
								<div class="col-md-3 col-lg-3 col-sm-6">
									<div class="counter col_third">
										<img src="images\icons\transformation.png" width="60px" alt="">
										<div>
										<span class="timer count-title count-number" data-to="250" data-speed="1500"></span><span style="font-size:4rem">M $</span>
										</div>
										<p class="count-text ">Transactions</p>
									</div>
								</div>
								<div class="col-md-3 col-lg-3 col-sm-6">
									<div class="counter col_third end">
										<img src="images\icons\user-experience.png" width="60px" alt="">
										<div>
										<span class="timer count-title count-number" data-to="1" data-speed="1500"></span><span style="font-size:4rem">M +</span>
										</div>
										<p class="count-text ">Happy Peoples</p>
									</div>
								</div> 
							</div>	
						</div>
					</div>
				</div>
			</div>
		 
		</div>
		<!-- End content -->
		<?php include("footer.php");?>
	</div>
	<!-- End Container -->
 
	<script type="text/javascript">

		var tpj=jQuery;
		tpj.noConflict();

		tpj(document).ready(function() {

		if (tpj.fn.cssOriginal!=undefined)
			tpj.fn.css = tpj.fn.cssOriginal;

			var api = tpj('.fullwidthbanner').revolution(
				{
					delay:8000,
					startwidth:1170,
					startheight:500,
					onHoverStop:"off",						 
					thumbWidth:100,							 
					thumbHeight:50,
					thumbAmount:3,

					hideThumbs:0,
					navigationType:"none",				 
					navigationArrows:"solo",			 

					navigationStyle:"round",				 

					navigationHAlign:"center",				 
					navigationVAlign:"bottom",				 
					navigationHOffset:30,
					navigationVOffset: 40,

					soloArrowLeftHalign:"left",
					soloArrowLeftValign:"center",
					soloArrowLeftHOffset:0,
					soloArrowLeftVOffset:0,

					soloArrowRightHalign:"right",
					soloArrowRightValign:"center",
					soloArrowRightHOffset:0,
					soloArrowRightVOffset:0,

					touchenabled:"on",	
					stopAtSlide:-1,							
					stopAfterLoops:-1,						
					hideCaptionAtLimit:0,					
					hideAllCaptionAtLilmit:0,				
					hideSliderAtLimit:0,					 
					fullWidth:"on",
					shadow:1								 

				});
 
					api.bind("revolution.slide.onloaded",function (e) {


						jQuery('.tparrows').each(function() {
							var arrows=jQuery(this);

							var timer = setInterval(function() {

								if (arrows.css('opacity') == 1 && !jQuery('.tp-simpleresponsive').hasClass("mouseisover"))
								  arrows.fadeOut(300);
							},3000);
						})

						jQuery('.tp-simpleresponsive, .tparrows').hover(function() {
							jQuery('.tp-simpleresponsive').addClass("mouseisover");
							jQuery('body').find('.tparrows').each(function() {
								jQuery(this).fadeIn(300);
							});
						}, function() {
							if (!jQuery(this).hasClass("tparrows"))
								jQuery('.tp-simpleresponsive').removeClass("mouseisover");
						})
					});
			});
	</script>
	<script>
		jQuery(function(){ 
			DevSolutionSkill.init('circle'); 
			DevSolutionSkill.init('circle2'); 
			DevSolutionSkill.init('circle3'); 
			DevSolutionSkill.init('circle4'); 
			DevSolutionSkill.init('circle5'); 
			DevSolutionSkill.init('circle6');
		});
	</script>
</body>
</html>