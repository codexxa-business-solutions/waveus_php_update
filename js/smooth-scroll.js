 

var $ = jQuery.noConflict();

$(document).ready(function($) {
	"use strict";

	/*-------------------------------------------------*/
	/* =   Smooth scroll
	/*-------------------------------------------------*/

	$('#container').imagesLoaded(function(){
		function getTargetTop(elem){
			
		 
			var id = elem.attr("href");

			var offset = 80;

	 
			return $(id).offset().top - offset;
		}


		var elemHref = $('.navbar-right a[href^="#"]');

		elemHref.click(function(event) {
			
			var target = getTargetTop($(this));

			$('html, body').animate({scrollTop:target}, 500);

			event.preventDefault();
		});

		var sections = $('.navbar-right a[href^="#"]');

		function checkSectionSelected(scrolledTo){
			
			var threshold = 100;

			var i;

			for (i = 0; i < sections.length; i++) {
				
				var section = $(sections[i]);

				var target = getTargetTop(section);
				
				if (scrolledTo > target - threshold && scrolledTo < target + threshold) {

					sections.removeClass("active");

					section.addClass("active");
				}

			}
		}

		checkSectionSelected($(window).scrollTop());

		$(window).scroll(function(){
			checkSectionSelected($(window).scrollTop());
		});

	});

	/* ---------------------------------------------------------------------- */
	/*	Header animate after scroll
	/* ---------------------------------------------------------------------- */

	(function() {

		var docElem = document.documentElement,
			didScroll = false,
			changeHeaderOn = $(window).height() - 30;
			document.querySelector( '.navbar' );
		function init() {
			window.addEventListener( 'scroll', function() {
				if( !didScroll ) {
					didScroll = true;
					setTimeout( scrollPage, 250 );
				}
			}, false );
		}
		
		function scrollPage() {
			var sy = scrollY();
			if ( sy >= changeHeaderOn ) {
				$( '.navbar' ).addClass('active');
			}
			else {
				$( '.navbar' ).removeClass('active');
			}
			didScroll = false;
		}
		
		function scrollY() {
			return window.pageYOffset || docElem.scrollTop;
		}
		
		init();
		
	})();
});

